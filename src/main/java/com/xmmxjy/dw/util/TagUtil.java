package com.xmmxjy.dw.util;

import com.xmmxjy.common.util.SpringUtils;
import com.xmmxjy.dw.entity.TagEntity;
import com.xmmxjy.dw.service.TagService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xmm on 2016/12/29.
 */
public class TagUtil {

    public static  List<TagEntity> tagList = null;

    @PostConstruct
    public void init(){
        TagService  tagService = SpringUtils.getBean("tagService");
        tagList = tagService.selectAll();
    }

    public static List<TagEntity> getTagList(){
        return tagList;
    }
}
