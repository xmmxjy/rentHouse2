package com.xmmxjy.hbase.entity;

import java.math.BigDecimal;

/**
 * Created by xmm on 2017/2/16.
 */
public class DwCategory implements Comparable<DwCategory>{

    private String categoryCode;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getCategroyWeight() {
        return categroyWeight;
    }

    public void setCategroyWeight(double categroyWeight) {
        this.categroyWeight = categroyWeight;
    }

    private String categoryName;

    private double categroyWeight;

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }


    public DwCategory(String categoryCode, String categoryName) {
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
    }
    public DwCategory() {

    }
    @Override
    public String toString() {
        return "DwCategory{" +
                "categoryCode='" + categoryCode + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", categroyWeight=" + categroyWeight +
                '}';
    }

    @Override
    public int compareTo(DwCategory o) {
        return new BigDecimal(o.getCategroyWeight()).compareTo(new BigDecimal(this.getCategroyWeight()));

    }
}
