<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">用户画像展示</div>
            <div class="wrapper wrapper-content animated">
                <div class="row">
                <div class="col-sm-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>性别分布</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="echarts" id="sex"></div>
                        </div>
                    </div>
                </div>
                    <div class="col-sm-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>年龄分布</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="echarts" id="age"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>地域分布</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="echarts" id="map" style="height: 600px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript" src="${basePath}/plug-in-ui/echarts/echarts.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/echarts/china.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/echarts/echarts-wordcloud.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>
<script type="text/javascript">

    //性别分布
    var sex = ${sex!};
    var sexData = new Array();
    console.log(sex);
    if (sex != "") {
    var sexChart = echarts.init(document.getElementById('sex'));
    var sexOption = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'left',
            data:['男','女','未知']
        },
        series: [
            {
                name:'性别分布',
                type:'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data:sex
            }
        ]
    };
    sexChart.setOption(sexOption);
    } else {
        $("#sex").css("display","none");
    }

    var age = ${age!};
    if (age != "") {
        var ageChart = echarts.init(document.getElementById('age'));
        var ageOption = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                x: 'left',
                data:['18岁以下','18到30岁','30到40岁','40到50岁','50到60岁','60岁以上']
            },
            series: [
                {
                    name:'年龄分布',
                    type:'pie',
                    radius: ['50%', '70%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:age
                }
            ]
        };
        ageChart.setOption(ageOption);
    }
    var map = ${map!};
    var s = echarts.init(document.getElementById("map")),
            c = {
                title: {
                    text: "地域",
                    x: "center"
                },
                tooltip: {
                    trigger: "item"
                },
                legend: {
                    orient: "vertical",
                    x: "left",
                    data: ["各地域人数"]
                },
                dataRange: {
                    min: 0,
                    max: 2500,
                    x: "left",
                    y: "bottom",
                    text: ["高", "低"],
                    calculable: !0
                },
                /*toolbox: {
                    show: !0,
                    orient: "vertical",
                    x: "right",
                    y: "center",
                    feature: {
                        mark: {
                            show: !0
                        },
                        dataView: {
                            show: !0,
                            readOnly: !1
                        },
                        restore: {
                            show: !0
                        },
                        saveAsImage: {
                            show: !0
                        }
                    }
                },*/
                roamController: {
                    show: !0,
                    x: "right",
                    mapTypeControl: {
                        china: !0
                    }
                },
                series: [{
                    name: "各地域人数",
                    type: "map",
                    mapType: "china",
                    roam: !1,
                    itemStyle: {
                        normal: {
                            label: {
                                show: !0
                            }
                        },
                        emphasis: {
                            label: {
                                show: !0
                            }
                        }
                    },
                    data: map
                }]
            };
    s.setOption(c);
</script>