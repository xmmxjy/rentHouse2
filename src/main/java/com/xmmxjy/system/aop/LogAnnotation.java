package com.xmmxjy.system.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by xmm on 2017/3/13.
 * aop日志注解
 */
@Retention(RetentionPolicy.RUNTIME)//注解会在class中存在，运行时可通过反射获取
@Target(ElementType.METHOD)//目标是方法
public @interface LogAnnotation {
    //分为业务或者系统 1代表业务，0代表系统
    String eventType() default "";
    //操作类型 增删改查等
    String operateType() default "";
    //操作模块 是user，role
    String operateModule() default "";
}
