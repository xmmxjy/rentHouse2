package com.xmmxjy.hbase.entity;

import java.util.List;

/**
 * Created by xmm on 2017/2/16.
 */
public class DwUser {
    //客户端ID
    private String clientId;
    //手机号
    private String phone;
    //主题词列表
    private List<DwTopic> topicList;
    //分类列表
    private List<DwCategory> categoryList;
    //渠道列表
    private List<DwChannel> channelList;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<DwTopic> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<DwTopic> topicList) {
        this.topicList = topicList;
    }

    public List<DwCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<DwCategory> categoryList) {
        this.categoryList = categoryList;
    }

    public List<DwChannel> getChannelList() {
        return channelList;
    }

    public void setChannelList(List<DwChannel> channelList) {
        this.channelList = channelList;
    }

    @Override
    public String toString() {
        return "DwUser{" +
                "phone='" + phone + '\'' +
                '}';
    }
}
