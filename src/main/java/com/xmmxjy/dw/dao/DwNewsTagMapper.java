package com.xmmxjy.dw.dao;

import com.xmmxjy.dw.entity.DwNewsTag;
import tk.mybatis.mapper.common.Mapper;

public interface DwNewsTagMapper extends Mapper<DwNewsTag> {
}