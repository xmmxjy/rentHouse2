package com.xmmxjy.dw.service;

import com.xmmxjy.common.service.BaseService;
import com.xmmxjy.dw.entity.DwNewsTag;
import com.xmmxjy.dw.entity.NewsEntity;
import com.xmmxjy.dw.entity.Tag;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/
public interface NewsService extends BaseService<NewsEntity>{


    void saveTag(Long id,Tag[] data);

    List<DwNewsTag> selectTagById(long newsId);
}
