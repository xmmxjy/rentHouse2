<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">新增</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/hbaseuser/userSync.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <div class="form-group mno">
                        <label for="userId" class="col-sm-2 control-label" style="text-align:left;">用户ID</label>
                        <div class="col-sm-8">
                            <input type="text" name="id" id="id" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="type" class="col-sm-2 control-label" style="text-align:left;">类型</label>
                        <div class="col-sm-8">
                            <select name="type" id="type"  class="form-control" placeholder="类型">
                                <option value="insert">添加</option>
                                <option value="update">修改</option>
                                <option value="delete">删除</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="username" class="col-sm-2 control-label" style="text-align:left;">用户名</label>
                        <div class="col-sm-8">
                            <input type="text" name="username" id="username" class="form-control"  />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="phone" class="col-sm-2 control-label" style="text-align:left;">手机号</label>
                        <div class="col-sm-8">
                            <input type="text" name="phone" id="phone" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="email" class="col-sm-2 control-label" style="text-align:left;">邮箱</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" id="email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="birthday" class="col-sm-2 control-label" style="text-align:left;">生日</label>
                        <div class="col-sm-8">
                            <input type="text" name="birthday" id="birthday" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="sex" class="col-sm-2 control-label" style="text-align:left;">性别</label>
                        <div class="col-sm-8">
                            <input type="text" name="sex" id="sex" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="mobilePhone" class="col-sm-2 control-label" style="text-align:left;">家乡</label>
                        <div class="col-sm-8">
                            <input type="text" name="home" id="home" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="currentRegion" class="col-sm-2 control-label" style="text-align:left;">年龄</label>
                        <div class="col-sm-8">
                            <input type="text" name="age" id="age" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="homeRegion" class="col-sm-2 control-label" style="text-align:left;">所在地</label>
                        <div class="col-sm-8">
                            <input type="text" name="location" id="location" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="school" class="col-sm-2 control-label" style="text-align:left;">手机设备号</label>
                        <div class="col-sm-8">
                            <input type="text" name="imei" id="imei" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="isMarry" class="col-sm-2 control-label" style="text-align:left;">婚恋</label>
                        <div class="col-sm-8">
                            <input type="text" name="isMarry" id="isMarry" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="constellation" class="col-sm-2 control-label" style="text-align:left;">星座</label>
                        <div class="col-sm-8">
                            <input type="text" name="constellation" id="constellation" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="occupation" class="col-sm-2 control-label" style="text-align:left;">职业</label>
                        <div class="col-sm-8">
                            <input type="text" name="profession" id="profession" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="interest" class="col-sm-2 control-label" style="text-align:left;">兴趣爱好</label>
                        <div class="col-sm-8">
                            <input type="text" name="interest" id="interest" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="model" class="col-sm-2 control-label" style="text-align:left;">手机型号</label>
                        <div class="col-sm-8">
                            <input type="text" name="model" id="model" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="os" class="col-sm-2 control-label" style="text-align:left;">手机操作系统</label>
                        <div class="col-sm-8">
                            <input type="text" name="os" id="os" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group mno">
                        <label for="operator" class="col-sm-2 control-label" style="text-align:left;">运营商</label>
                        <div class="col-sm-8">
                            <input type="text" name="operator" id="operator" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="keyWords" class="col-sm-2 control-label" style="text-align:left;">搜索关键词</label>
                        <div class="col-sm-8">
                            <input type="text" name="keyWords" id="keyWords" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="questionList" class="col-sm-2 control-label" style="text-align:left;">搜索问题列表</label>
                        <div class="col-sm-8">
                            <input type="text" name="questionList" id="questionList" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="typeList" class="col-sm-2 control-label" style="text-align:left;">推荐的达人类型</label>
                        <div class="col-sm-8">
                            <input type="text" name="typeList" id="typeList" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/user/list.do');">返回</button>
                            <button type="button" class="btn btn-primary" id="formSubmit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>