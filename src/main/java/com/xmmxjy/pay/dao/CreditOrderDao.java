package com.xmmxjy.pay.dao;


import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.pay.entity.CreditOrderEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author：xmm
* @version:1.0
*/
public interface CreditOrderDao extends MyMapper<CreditOrderEntity> {

    List<CreditOrderEntity> selectByUserId(@Param("appuserId") Integer appuserId);
}

