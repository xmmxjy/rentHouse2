package com.xmmxjy.dw.util;

import me.xiaosheng.word2vec.Word2Vec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by xmm on 2016/12/29.
 */
public class Word2VecUtil {

    private static Logger logger = LoggerFactory.getLogger(Word2VecUtil.class);

    private static Word2Vec vec = new Word2Vec();

    @PostConstruct
    public void init(){

        try {
            vec.loadGoogleModel("D:/eclipseWork/WordCount/src/data/wiki_chinese_word2vec(Google).model");
            logger.info("google model load!");
        } catch (IOException e) {
           logger.error(e.getMessage(),e);
        }
    }

    public static Word2Vec get(){
        return vec;
    }

}
