package com.xmmxjy.dw.util;

import com.xmmxjy.common.util.DateUtil;
import com.xmmxjy.common.util.HttpClientUtil;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.common.util.XmlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xmm on 2017/1/18.
 */
public class InterfaceTest {

    private final static Logger log = LoggerFactory.getLogger(InterfaceTest.class);
    /**
     * 用户动作模拟测试
     */
    public static Map<String,String> userAction(String url,String id, String msisdn, String imsi, String imei, String clientDeviceNumber, String action, String referer, String reason){
        if (url == null || url.trim().equals("")) {
             url = "http://hadoop2:8990/Recommend/syncAction.cp";
        }
        Map<String,String> params = new HashMap<String,String>();
        params.put("version","1.0");
        params.put("uuid", Tools.getUUID());
        params.put("time", DateUtil.getCurrentTime());
        params.put("msisdn",msisdn);
        params.put("imsi",imsi);
        params.put("imei",imei);
        params.put("client_device_number",clientDeviceNumber);
        params.put("client_version","");
        params.put("os","");
        params.put("city","");
        params.put("id",id);
        params.put("action",action);
        params.put("referer",referer);
        params.put("ua","");
        params.put("reason",reason);

        String xml = HttpClientUtil.getInstance().sendHttpPost(url,params);
        Map<String,String> resultMap = XmlUtil.xmlToMap(xml);
        log.info( "resultMap : " + resultMap);
        return resultMap;
    }

    /**
     * 用户偏好测试
     * @param url
     * @param msisdn
     * @param imsi
     * @param imei
     * @param clientDeviceNumber
     * @param channels
     */
    public static Map<String,String> userPreference(String url, String msisdn, String imsi, String imei, String clientDeviceNumber, String channels) {
        if (url == null || url.trim().equals("")) {
            url = "http://hadoop2:8990/Recommend/syncUserPreference.cp";
        }
        Map<String,String> params = new HashMap<String,String>();
        params.put("version","1.0");
        params.put("uuid", Tools.getUUID());
        params.put("time", DateUtil.getCurrentTime());
        params.put("msisdn",msisdn);
        params.put("imsi",imsi);
        params.put("imei",imei);
        params.put("client_device_number",clientDeviceNumber);
        params.put("channels",channels);
        String xml = HttpClientUtil.getInstance().sendHttpPost(url, params);
        Map<String,String> resultMap = XmlUtil.xmlToMap(xml);
        log.info( "resultMap : " + resultMap);
        return resultMap;
    }

    /**
     * 新闻同步接口
     * @param url
     * @param operType
     * @param id
     * @param title
     * @param channel
     * @param category
     * @param keywords
     * @param content
     * @param newsUrl
     * @param pubTime
     * @param icon
     * @param editorPush
     */
    public static Map<String,String> newsSync(String url, String operType, String id, String title, String channel, String category,
    String keywords, String content, String newsUrl, String pubTime, String icon,String editorPush) {
        if (url == null || url.trim().equals("")) {
            url = "http://hadoop3:8990/new_interface/syncNews.cp";
        }
        Map<String,String> params = new HashMap<String,String>();
        params.put("version","1.0");
        params.put("uuid", Tools.getUUID());
        params.put("oper_type",operType);
        params.put("id",id);
        params.put("site","www.sgcctop.com");
        params.put("title",title);
        params.put("channel",channel);
        params.put("category",category);
        params.put("keywords",keywords);
        params.put("content",content);
        if (newsUrl == null  || newsUrl.trim().equals("")) {
            newsUrl = "www.sgcctop.com/news/" + id;
        }
        params.put("url",newsUrl);
        params.put("pub_time",pubTime);
        params.put("icon",icon);
        params.put("editor_push",editorPush);
        String xml = HttpClientUtil.getInstance().sendHttpPost(url, params);
        Map<String,String> resultMap = XmlUtil.xmlToMap(xml);
        log.info( "resultMap : " + resultMap);
        return resultMap;
    }


}
