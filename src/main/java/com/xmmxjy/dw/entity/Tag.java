package com.xmmxjy.dw.entity;

public class Tag implements Comparable<Tag>{

	private long id;
	private String name;
	private float score;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}

	@Override
	public int compareTo(Tag o) {
		return Float.compare(this.score, o.score);
	}
	
	public Tag() {
		
	}
	
	public Tag(float score) {
		this.score = score;
	}
	public Tag(String name,float score) {
		this.name = name;
		this.score = score;
	}
	public Tag(long id,String name,float score) {
		this.id = id;
		this.name = name;
		this.score = score;
	}
	@Override
	public String toString() {
		return name + "|";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(score);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(score) != Float.floatToIntBits(other.score))
			return false;
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
