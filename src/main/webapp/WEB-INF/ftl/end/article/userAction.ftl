<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">用户动作测试</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/article/userAction.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <div class="form-group mno">
                        <label for="url" class="col-sm-2 control-label" style="text-align:left;">url</label>
                        <div class="col-sm-8">
                            <input type="text" name="url" id="url" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="msisdn" class="col-sm-2 control-label" style="text-align:left;">手机号</label>
                        <div class="col-sm-8">
                            <input type="text" name="msisdn" id="msisdn" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="imsi" class="col-sm-2 control-label" style="text-align:left;">imsi</label>
                        <div class="col-sm-8">
                            <input type="text" name="imsi" id="imsi" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="imei" class="col-sm-2 control-label" style="text-align:left;">imei</label>
                        <div class="col-sm-8">
                            <input type="text" name="imei" id="imei" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="clientDeviceNumber" class="col-sm-2 control-label" style="text-align:left;">clientDeviceNumber</label>
                        <div class="col-sm-8">
                            <input type="text" name="clientDeviceNumber" id="clientDeviceNumber" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="id" class="col-sm-2 control-label" style="text-align:left;">新闻ID</label>
                        <div class="col-sm-8">
                            <input type="text" name="id" id="id" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="action" class="col-sm-2 control-label" style="text-align:left;">action</label>
                        <div class="col-sm-8">
                            <select id="action" name="action" class="form-control">
                                <option value="browse">浏览</option>
                                <option value="collect">收藏</option>
                                <option value="praise">点赞</option>
                                <option value="forward">转发</option>
                                <option value="hate">不感兴趣</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="referer" class="col-sm-2 control-label" style="text-align:left;">referer</label>
                        <div class="col-sm-8">
                            <input type="text" name="referer" id="referer" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="reason" class="col-sm-2 control-label" style="text-align:left;">reason</label>
                        <div class="col-sm-8">
                            <input type="text" name="reason" id="reason" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/article/list.do');">返回</button>
                            <button type="submit" class="btn btn-primary">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>