package com.xmmxjy.dw.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.dw.dao.NewsDao;
import com.xmmxjy.dw.entity.DwNewsTag;
import com.xmmxjy.dw.entity.NewsEntity;
import com.xmmxjy.dw.entity.Tag;
import com.xmmxjy.dw.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/

@Service("newsService")
public class NewsServiceImpl extends BaseServiceImpl<NewsEntity> implements NewsService {
    @Autowired
    private NewsDao newsDao;

    @Override
    public void saveTag(Long id,Tag[] data) {
        if (data != null && data.length > 0) {
            for (Tag tag : data) {
                float b = (float)(Math.round(tag.getScore()*100))/100;
                newsDao.saveTag(id, tag.getId(),b);
            }
        }
    }

    @Override
    public List<DwNewsTag> selectTagById(long newsId) {
        return newsDao.selectTagById(newsId);
    }
}
