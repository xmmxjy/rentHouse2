
package com.xmmxjy.common.util;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.BeanUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class ContextHolderUtils {
    public static final String LOCAL_CLINET_USER = "LOCAL_CLINET_USER";
    public static final String OPERATION_CODES = "operationCodes";
    public static final String DATA_RULE_CODES = "dataRulecodes";
    public static final String MENU_DATA_AUTHOR_RULE_SQL = "MENU_DATA_AUTHOR_RULE_SQL";

    public ContextHolderUtils() {
    }

    public static HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }

    public static HttpSession getSession() {
        HttpSession session = getRequest().getSession();
        return session;
    }



    public static Set<String> getPageSelectOperationCodes() {
        Set operationCodes = (Set)getRequest().getAttribute("operationCodes");
        return operationCodes;
    }

    public static String getPageDataAuthorSQL() {
        String pageDataAuthorSQL = (String)getRequest().getAttribute("MENU_DATA_AUTHOR_RULE_SQL");
        return pageDataAuthorSQL;
    }

    public static Set<String> getPageDataAuthorCodes() {
        Set operationCodes = (Set)getRequest().getAttribute("dataRulecodes");
        return operationCodes;
    }

    public static String clearStyleWithContent(String _content) {
        if ((_content != null) && (!"".equals(_content))) {
            _content = _content.replaceAll("'", "\"").replaceAll("&nbsp;", "")
                    .replaceAll("<br//s*>", "</p><p>")
                    .replaceAll("&ldquo;", "").replaceAll("&rdquo;", "")
                    .replaceAll("&nbsp;", "").replaceAll("&mdash;", "")
                    .replaceAll("&hellip;", "").replaceAll("&hellip;", "")
                    .replaceAll("&middot;", "").replaceAll("&rsquo;", "")
                    .replaceAll("&lsquo;", "").replaceAll("\r", "").replaceAll(
                            "\n", "").replaceAll("\t", "").trim();
            //去掉p标签的样式,并增加自定义类
            _content = _content.replaceAll("<[p|P][\\s\\S]*?>", "");
            //去掉span标签的样式,并增加自定义类
            _content = _content.replaceAll("<span[\\s\\S]*?>", "");
            //去掉div标签的样式,并增加自定义类
            _content = _content.replaceAll("<div[\\s\\S]*?>", "");

            String regEx_img = "<img[^>]*>";
            Pattern p_image = Pattern.compile(regEx_img, 2);
            Matcher m_image = p_image.matcher(_content);
            int i = 0;
            while (m_image.find()) {
                String _img = m_image.group(0);
                String img_i = "IMAGE_" + i;
                StringBuffer buff = new StringBuffer();
                _content = _content.replace(_img, img_i);
                System.out.println(_content);
                Matcher m = Pattern.compile("\\bsrc\\b=\"?(.*?)(\"|>|\\s+)")
                        .matcher(_img);
                while (m.find()) {
                    String _src = m.group(1);
                    System.out.println(_src);
                    buff.append("<img src=\"").append(_src).append("\"")
                            .append(" class=\"content_img\" />");
                }
                _content = _content.replace(img_i, buff.toString());
                buff.setLength(0);
                i++;
            }
        }

        return _content;
    }
    public static String clearHtmlTag(String inputString) {
        if (inputString == null)
            return null;
        String htmlStr = inputString;
        String textStr = "";
        try {
            String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>";

            String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";

            String regEx_html = "<[^>]+>";

            String regEx_special = "\\&[a-zA-Z]{1,10};";

            Pattern p_script = Pattern.compile(regEx_script);
            Matcher m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll("");
            Pattern p_style = Pattern.compile(regEx_style);
            Matcher m_style = p_style.matcher(htmlStr);
            htmlStr = m_style.replaceAll("");
            Pattern p_html = Pattern.compile(regEx_html);
            Matcher m_html = p_html.matcher(htmlStr);
            htmlStr = m_html.replaceAll("");
            Pattern p_special = Pattern.compile(regEx_special);
            Matcher m_special = p_special.matcher(htmlStr);
            htmlStr = m_special.replaceAll("");
            textStr = htmlStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return textStr;
    }

    /**
     * 去除空格、回车、换行符、制表符
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
}
