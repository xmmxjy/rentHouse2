package com.xmmxjy.system.dao;


import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.system.entity.DictDataEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Map;

/**
* @author：xmm
* @version:1.0
*/
public interface DictDataDao extends MyMapper<DictDataEntity> {
//    @Sql("select * from rh_dict_data where dict_type_id in (select id from rh_dict_type where code = #{code}))")
    List<DictDataEntity> selectByCode(@Param("code") String code);

    String getDictDataName(@Param("code")String code,@Param("value") String value);
}

