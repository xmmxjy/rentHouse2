package com.xmmxjy.dw.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.dw.dao.UserDao;
import com.xmmxjy.dw.entity.DwUserTag;
import com.xmmxjy.dw.entity.UserEntity;
import com.xmmxjy.dw.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/

@Service("dwUserService")
public class UserServiceImpl extends BaseServiceImpl<UserEntity> implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public DwUserTag selectTag(String userId, long tagId) {
        return userDao.selectTag(userId,tagId);
    }
}
