package com.xmmxjy.hbase.entity;

import java.math.BigDecimal;

/**
 * Created by xmm on 2017/2/16.
 */
public class DwChannel implements Comparable<DwChannel>{

    private String channelName;

    private double channelWeight;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public double getChannelWeight() {
        return channelWeight;
    }

    public void setChannelWeight(double channelWeight) {
        this.channelWeight = channelWeight;
    }

    @Override
    public String toString() {
        return "DwChannel{" +
                "channelName='" + channelName + '\'' +
                ", channelWeight=" + channelWeight +
                '}';
    }

    @Override
    public int compareTo(DwChannel o) {
        return new BigDecimal(o.getChannelWeight()).compareTo(new BigDecimal(this.getChannelWeight()));
    }
}

