package com.xmmxjy.dw.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.dw.dao.TagDao;
import com.xmmxjy.dw.entity.TagEntity;
import com.xmmxjy.dw.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/

@Service("tagService")
public class TagServiceImpl extends BaseServiceImpl<TagEntity> implements TagService {
    @Autowired
    private TagDao tagDao;

}
