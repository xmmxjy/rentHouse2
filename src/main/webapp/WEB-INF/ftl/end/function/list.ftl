<!DOCTYPE html>
<html >
<#include "/end/include/head.ftl"/>
<link href="${basePath}/plug-in-ui/treetable/default/treeTable.css" rel="stylesheet" type="text/css" />
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">列表</div>
                <div class="panel-body">
                    <ul id="functionTree" class="ztree" style="margin-top:0; width:200px;"></ul>
                </div>
            </div>
        </div>


        <div  class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">菜单列表</div>
                    <div class="panel-body">
                        <div id="legend">
                            <legend  class="le">
                            <@shiro.hasPermission name="cms.channel.add"><button type="button" class="btn btn-primary" onclick="addFunction();">新增</button></@shiro.hasPermission>
                                <#--<button type="button" class="btn btn-primary" onclick="javascript:getChannel(null);" >刷新</button>-->
                            </legend>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <th>菜单名称</th>
                                <th>菜单级别</th>
                                <th>顺序</th>
                                <th>权限</th>
                                <th>操作</th>
                            </thead>
                            <tbody id="functionJson">
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath}/plug-in-ui/treetable/jquery.treeTable.min.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/zTree/jquery.ztree.core.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/zTree/jquery.ztree.excheck.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/zTree/jquery.ztree.exedit.js"></script>
<script type="text/javascript">

    function zTreeOnClick(event, treeId, treeNode){
        console.log(treeNode.isParent);
        /*if (treeNode.isParent) {
            getChannel(treeNode.id);
        }*/
        getChannel(treeNode.id);
    }

    function getChannel(id) {
        $.getJSON("${basePath}/function/functionList.do",{id:id},function(data){
            //console.log(data);
            var sum ="";
            $.each(data.functionList,function (i,info) {
                if (data.edit) {
                    var addTd = "<a href=\"javascript:void(0)\" onclick=\"javascript:doUrl('${basePath}/function/toEdit.do?id=" + info.id + "')\" >编辑</a>";
                }
                if (data.detail) {
                    var detailTd = "<a href=\"javascript:void(0)\" onclick=\"javascript:doUrl('${basePath}/function/toDetail.do?id=" + info.id + "')\" >详情</a>";
                }
                if (data.delete) {
                    var deleteTd = "<a href=\"javascript:void(0)\" onclick=\"javascript:delData('${basePath}/function/doDelete.do?id=" + info.id + "')\" >删除</a>";
                }
                var levelName = "一级菜单";
                if (info.functionlevel == '1') {
                    levelName = "一级菜单";
                } else if(info.functionlevel == '2'){
                    levelName = "二级菜单";
                } else if (info.functionlevel == '3') {
                    levelName = "按钮";
                }
                var html = "<tr>"
                            + "<td>" + info.functionname + "</td>"
                            + "<td>" + levelName + "</td>"
                            + "<td>" +  info.functionorder + "</td>"
                            + "<td>" + info.permission + "</td>"
                            + "<td class=\"last\">" + addTd + detailTd + deleteTd + "</td>"
                            + "</tr>";
                sum = sum +　html;
            });

            $("#functionJson").empty().append(sum);
        })
    }
    var setting = {

        view: {
            //addHoverDom: addHoverDom,
            //removeHoverDom: removeHoverDom,
            dblClickExpand: false
        },

        data: {
            key: {
                name: "functionname"
            },
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentFunctionId",
                rootPId: null
            }
        },callback: {
            onClick: zTreeOnClick
        }
    };



    var zNodes;
    $(document).ready(function() {
        $.ajax({
            async: false,
            cache: false,
            type: 'POST',
            dataType: "json",
            url: '${basePath}/function/tree.do?level=2',//请求的action路径
            error: function () {//请求失败处理函数
                alert('请求失败');
            },
            success: function (data) { //请求成功后处理函数。
                zNodes = data;   //把后台封装好的简单Json格式赋给zNodes
                //console.log(zNodes);
            }
        });
        zTree.init($("#functionTree"), setting, zNodes);
        var treeObj =  zTree.getZTreeObj("functionTree");
        var parentFunctionId = "${parentFunctionId!}";
        if (parentFunctionId != '') {
            getChannel(parentFunctionId)
            //var nodes = treeObj.getNodes();
            var nodes = getAllNodes(treeObj);
            console.log(nodes)
            if (nodes.length > 0) {
                for (var i = 0;i < nodes.length; i++) {
                    if (nodes[i].id == parentFunctionId) {
                        treeObj.selectNode(nodes[i]);
                    }
                }
            }
        } else {
            var nodes = treeObj.getNodes();
            if (nodes.length > 0) {
                treeObj.selectNode(nodes[0]);
                var firstNode = nodes[0].id;
                getChannel(firstNode);
            } else {
                getChannel(null);
            }
        }
        treeObj.expandAll(true);
    });
    function addFunction(){
        var treeObj =  zTree.getZTreeObj("functionTree");
        var nodes = treeObj.getSelectedNodes();
        if (nodes.length > 0) {
            var id = nodes[0].id;
            doUrl('${basePath}/function/toAdd.do?parentFunctionId='+id);
        } else {
            doUrl('${basePath}/function/toAdd.do');
        }
    }
    function getAllNodes(treeObj) {
        var node = treeObj.getNodes();
        var nodes = treeObj.transformToArray(node);
        return nodes;
    }
</script>
</html>