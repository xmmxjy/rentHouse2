<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">新增</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/user/doAdd.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <div class="form-group mno">
                        <label for="userId" class="col-sm-2 control-label" style="text-align:left;">用户ID</label>
                        <div class="col-sm-8">
                            <input type="text" name="userId" id="userId" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="username" class="col-sm-2 control-label" style="text-align:left;">用户名</label>
                        <div class="col-sm-8">
                            <input type="text" name="username" id="username" class="form-control"  datatype="*" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="email" class="col-sm-2 control-label" style="text-align:left;">邮箱</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" id="email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="birthday" class="col-sm-2 control-label" style="text-align:left;">生日</label>
                        <div class="col-sm-8">
                            <input type="text" name="birthday" id="birthday" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="sex" class="col-sm-2 control-label" style="text-align:left;">性别</label>
                        <div class="col-sm-8">
                            <input type="text" name="sex" id="sex" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="mobilePhone" class="col-sm-2 control-label" style="text-align:left;">手机号码</label>
                        <div class="col-sm-8">
                            <input type="text" name="mobilePhone" id="mobilePhone" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="currentRegion" class="col-sm-2 control-label" style="text-align:left;">所在区域</label>
                        <div class="col-sm-8">
                            <input type="text" name="currentRegion" id="currentRegion" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="homeRegion" class="col-sm-2 control-label" style="text-align:left;">家乡</label>
                        <div class="col-sm-8">
                            <input type="text" name="homeRegion" id="homeRegion" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="school" class="col-sm-2 control-label" style="text-align:left;">所在学校</label>
                        <div class="col-sm-8">
                            <input type="text" name="school" id="school" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="isMarry" class="col-sm-2 control-label" style="text-align:left;">婚恋</label>
                        <div class="col-sm-8">
                            <input type="text" name="isMarry" id="isMarry" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="constellation" class="col-sm-2 control-label" style="text-align:left;">星座</label>
                        <div class="col-sm-8">
                            <input type="text" name="constellation" id="constellation" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="occupation" class="col-sm-2 control-label" style="text-align:left;">职业</label>
                        <div class="col-sm-8">
                            <input type="text" name="occupation" id="occupation" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="interest" class="col-sm-2 control-label" style="text-align:left;">兴趣爱好</label>
                        <div class="col-sm-8">
                            <input type="text" name="interest" id="interest" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/user/list.do');">返回</button>
                            <button type="button" class="btn btn-primary" id="formSubmit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>