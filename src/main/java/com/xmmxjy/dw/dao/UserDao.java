package com.xmmxjy.dw.dao;


import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.dw.entity.DwUserTag;
import com.xmmxjy.dw.entity.UserEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author：xmm
* @version:1.0
*/
@Repository(value = "dwUserDao")
public interface UserDao extends MyMapper<UserEntity> {

    DwUserTag selectTag(@Param("userId") String userId, @Param("tagId") long tagId);
}

