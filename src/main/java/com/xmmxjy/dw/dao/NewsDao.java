package com.xmmxjy.dw.dao;


import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.dw.entity.DwNewsTag;
import com.xmmxjy.dw.entity.NewsEntity;
import com.xmmxjy.dw.entity.Tag;
import com.xmmxjy.dw.entity.TagEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author：xmm
* @version:1.0
*/
public interface NewsDao extends MyMapper<NewsEntity> {

    void saveTag(@Param("id") Long id, @Param("tagId")Long tagId,@Param("score") float score);

    List<DwNewsTag> selectTagById(@Param("newsId")long newsId);
}

