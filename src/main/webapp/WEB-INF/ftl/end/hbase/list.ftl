<!DOCTYPE html>
<html >
<#include "/end/include/head.ftl"/>
<link href="${basePath}/plug-in-ui/treetable/default/treeTable.css" rel="stylesheet" type="text/css" />
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <form role="form" class="form-inline" action="${basePath}/hbaseuser/list.do" method="post"  id="formSubmit">
            <div  class="col-md-10" style="width:100%">
                <div class="panel panel-default">
                    <div class="panel-heading">列表</div>
                    <div class="panel-body">
                        <input name="pageNo" id="pageNo" value="${pageNo!}" type="hidden">
                        <input name="pageSize" id="pageSize" value="${pageSize!}" type="hidden">
                        <input name="startRow" id="startRow" value="${startRow!}" type="hidden">
                        <div class="search">
                            <div class="form-group col-sm-3">
                                <label for="id" class="control-label col-sm-3 line34">用户ID</label>
                                <div class="col-sm-8">
                                    <input type="text" name="id" id="id" value="${query.id!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="username" class="control-label col-sm-3 line34">用户名</label>
                                <div class="col-sm-8">
                                    <input type="text" name="username" id="username" value="${query.username!}" class="form-control">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">搜  索</button>
                            <div class="clearfix"></div>
                        </div>

                        <div id="legend">
                            <legend  class="le">
                            <@shiro.hasPermission name="dw.user.add"><button type="button" class="btn btn-primary" onclick="javascript:doUrl('${basePath}/user/toAdd.do');" >新增</button></@shiro.hasPermission>
                            </legend>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <th>用户ID</th>
                                <th>用户名</th>
                                <th>邮箱</th>
                                <th>生日</th>
                                <th>性别</th>
                                <th>手机号码</th>
                                <th>所在区域</th>
                                <th>家乡</th>
                                <th>所在学校</th>
                                <th>婚恋</th>
                                <th>星座</th>
                                <th>职业</th>
                                <th>兴趣爱好</th>
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <#list userList as info>
                            <tr>
                                <td>${info.id!}</td>
                                <td>${info.username!}</td>
                                <td>${info.email!}</td>
                                <td>${info.birthday!}</td>
                                <td>${info.sex!}</td>
                                <td>${info.mobilePhone!}</td>
                                <td>${info.currentRegion!}</td>
                                <td>${info.homeRegion!}</td>
                                <td>${info.school!}</td>
                                <td>${info.isMarry!}</td>
                                <td>${info.constellation!}</td>
                                <td>${info.occupation!}</td>
                                <td>${info.interest!}</td>
                                <td class="last">
                                <a href="javascript:void(0)" onclick="javascript:doUrl('${basePath}/hbaseuser/personas.do?id=${info.id}')">用户画像</a>
                                </td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <ul class="pagination" id="pagination1">
<#--
                                <li class="prev"><a href="javascriptscript:;" onclick="">上一页</a></li>
-->
                                <li class="next"><a href="javascript:;" onclick="nextPage()">下一页</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath}/plug-in-ui/treetable/jquery.treeTable.min.js"></script>

<script type="text/javascript">

    function nextPage(){
        document.getElementById('formSubmit').submit();
    }

    /*//当前页码
    var pageNo = ${pageNo};
    //当前页数
    var pages = 2;

    var visiblePages = pages;
    if (pages >= 10) {
        visiblePages = 10;
    }

    $.jqPaginator('#pagination1', {
        totalPages: pages,
        visiblePages: visiblePages,
        currentPage: pageNo,
        prev: '<li class="prev"><a href="javascript:;">上一页</a></li>',
        next: '<li class="next"><a href="javascript:;">下一页</a></li>',
        page: '<li class="page"><a href="javascript:;">{{page}}</a></li>',
        first: '<li class="next"><a href="javascript:;">首页</a></li>',
        last: '<li class="next"><a href="javascript:;">末页</a></li>',
        onPageChange: function (num, type) {
            console.log("num : " + num);
            console.log("type : " + type);
            if (type != "init") {
                //$('#p1').text(type + '：' + num);
                //document.getElementById('pageNo').value = num;
                document.getElementById('formSubmit').submit();
            }
        }
    });
    $(function(){
        var option = {
            theme:'default',
            expandLevel : 3,
            beforeExpand : function($treeTable, id) {
            },
            onSelect : function($treeTable, id) {
                window.console && console.log('onSelect:' + id);
            }

        };
        $('#treeTable1').treeTable(option);
    });*/
</script>
</html>