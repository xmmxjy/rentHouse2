package com.xmmxjy.dw.util;

import com.xmmxjy.common.util.SpringUtils;
import com.xmmxjy.dw.entity.DwNewsTag;
import com.xmmxjy.dw.entity.DwUserTag;
import com.xmmxjy.dw.service.NewsService;
import com.xmmxjy.dw.service.UserService;

import java.util.List;

/**
 * Created by xmm on 2016/12/30.
 */
public class LFMUtil {

    /**
     * 求用户对于某一个内容的兴趣度
     */
    public static double contentInterest(long newsId,String userId) {

        NewsService newsService = SpringUtils.getBean("newsService");

        UserService userService = SpringUtils.getBean("dwUserService");

        List<DwNewsTag> tagList = newsService.selectTagById(newsId);
        for (int i = 0 ; tagList.size() > 0; i++) {
            DwNewsTag nt = tagList.get(i);
            long tagId = nt.getTagId();
           DwUserTag userTag = userService.selectTag(userId,tagId);
        }
        return 0.0d;
    }
}
