package com.xmmxjy.system.directive;

import com.xmmxjy.common.util.SpringUtils;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.system.service.DictDataService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * Created by xmm on 2017/2/27.
 */
@Component(value = "dictDisplay")
public class DictDisplayDirective implements TemplateDirectiveModel {
    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        SimpleScalar paramValue = (SimpleScalar) map.get("value");
        SimpleScalar code = (SimpleScalar) map.get("code");
        String value = paramValue.getAsString();

        Writer out = environment.getOut();
        DictDataService dictDataService = (DictDataService) SpringUtils.getBean("dictDataService");
        String result = dictDataService.getDictDataName(code.getAsString(),value);
        if (Tools.isEmpty(result)) {
            result = "";
        }
        out.write(result);
    }
}
