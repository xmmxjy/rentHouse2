CREATE TABLE rh_credit_order
(
  id bigint(20) NOT NULL PRIMARY KEY COMMENT '订单ID',
  credit_num INT COMMENT '积分数值',
  money DOUBLE COMMENT '金额',
  subject VARCHAR(50) COMMENT '商品名称',
  subject_detail VARCHAR(200) COMMENT '商品详情',
  pay_type VARCHAR(50) COMMENT '支付方式',
  pay_os VARCHAR(10) COMMENT '支付的操作系统',
  order_status VARCHAR(20) COMMENT '订单状态',
  order_time datetime COMMENT '下单时间',
  pay_time datetime COMMENT '支付时间',
  appuser_id INT NOT NULL COMMENT '用户ID'
)

CREATE TABLE rh_global
(
  id VARCHAR(10) NOT NULL PRIMARY KEY COMMENT '全局配置ID',
  session_timeout INT COMMENT 'session超时时间设置',
  max_concurrency INT COMMENT '最大并发数'
);