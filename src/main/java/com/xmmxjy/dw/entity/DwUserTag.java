package com.xmmxjy.dw.entity;

import javax.persistence.*;

@Table(name = "dw_user_tag")
public class DwUserTag {
    /**
     * 标签ID
     */
    @Id
    @Column(name = "tag_id")
    private Long tagId;

    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private String userId;

    /**
     * 权重
     */
    private Double weight;

    /**
     * 获取标签ID
     *
     * @return tag_id - 标签ID
     */
    public Long getTagId() {
        return tagId;
    }

    /**
     * 设置标签ID
     *
     * @param tagId 标签ID
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取权重
     *
     * @return weight - 权重
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * 设置权重
     *
     * @param weight 权重
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }
}