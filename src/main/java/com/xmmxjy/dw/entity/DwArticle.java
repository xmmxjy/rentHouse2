package com.xmmxjy.dw.entity;

import javax.persistence.*;

@Table(name = "dw_article")
public class DwArticle {
    @Id
    private String id;

    @Column(name = "article_category_id")
    private String articleCategoryId;

    private String content;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return article_category_id
     */
    public String getArticleCategoryId() {
        return articleCategoryId;
    }

    /**
     * @param articleCategoryId
     */
    public void setArticleCategoryId(String articleCategoryId) {
        this.articleCategoryId = articleCategoryId;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }
}