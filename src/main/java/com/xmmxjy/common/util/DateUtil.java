package com.xmmxjy.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by xmm on 2017/1/17.
 */
public class DateUtil {

    public static String getCurrentTime(String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(new Date());
    }
    public static String getCurrentTime(){
        return getCurrentTime("yyyyMMddHHmmss");
    }

    public static Date stringToDate(String time,String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        try {
            if (time == null) {
                return new Date();
            }
            return df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Date stringToDate(String time) {
        return stringToDate(time, "yyyy-MM-dd HH:mm:ss");
    }

    public static String dateToString (Date date,String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    public static String dateToString (Date date){
        return dateToString(date, "yyyyMMddHHmmss");
    }
}
