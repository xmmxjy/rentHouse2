package com.xmmxjy.hbase.entity;

import java.util.List;

/**
 * Created by xmm on 2017/2/17.
 */
public class DwUserPage {
    //用户ID
    private String id;
    //客户端ID
    private String clientId;
    //用户名
    private String username;
    //性别
    private String sex;
    //归属地,根据地域推荐
    private String home;
    //出生年月日，根据年龄推荐
    private String birthday;
    //所在地，根据地域推荐
    private String location;
    //手机号
    private String phone;
    //邮箱
    private String email;
    //设备标识号
    private String imei;
    //手机操作系统
    private String os;
    //手机型号
    private String model;
    //运营商
    private String operator;
    //学历
    private String qualifications;
    //是否结婚(1为结婚，0为未婚)
    private String isMarry;
    //职业
    private String profession;
    //兴趣
    private String interest;
    //搜索历史关键词，可把该关键词当作主题词，推荐主题词对应的新闻,以分号分隔
    private String keyWords;
    //搜索的问题，可把该问题分词以后，当作主题词，推荐主题词对应的新闻,以分号分隔
    private String questionList;
    //关注的达人号的类型,可以当作偏好的频道推荐,以分号分隔
    private String typeList;
    //总积分
    private String sumPoint;
    //消耗了的积分
    private String consumePonit;
    //总收藏
    private String sumCollection;
    //提问的总数
    private String sumAsk;
    //回答问题的总数
    private String sumAnswer;
    //评论新闻的总数
    private String sumComment;
    //点赞新闻的总数
    private String sumLike;
    //阅读新闻的总数
    private String sumRead;
    //分享新闻的总数
    private String sumShare;
    //不感兴趣的新闻的总数
    private String sumUnInterested;
    //不喜欢的总数
    private String sumUnLike;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getQualifications() {
        return qualifications;
    }

    public void setQualifications(String qualifications) {
        this.qualifications = qualifications;
    }

    public String getIsMarry() {
        return isMarry;
    }

    public void setIsMarry(String isMarry) {
        this.isMarry = isMarry;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getQuestionList() {
        return questionList;
    }

    public void setQuestionList(String questionList) {
        this.questionList = questionList;
    }

    public String getTypeList() {
        return typeList;
    }

    public void setTypeList(String typeList) {
        this.typeList = typeList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSumPoint() {
        return sumPoint;
    }

    public void setSumPoint(String sumPoint) {
        this.sumPoint = sumPoint;
    }

    public String getConsumePonit() {
        return consumePonit;
    }

    public void setConsumePonit(String consumePonit) {
        this.consumePonit = consumePonit;
    }

    public String getSumCollection() {
        return sumCollection;
    }

    public void setSumCollection(String sumCollection) {
        this.sumCollection = sumCollection;
    }

    public String getSumAsk() {
        return sumAsk;
    }

    public void setSumAsk(String sumAsk) {
        this.sumAsk = sumAsk;
    }

    public String getSumAnswer() {
        return sumAnswer;
    }

    public void setSumAnswer(String sumAnswer) {
        this.sumAnswer = sumAnswer;
    }

    public String getSumComment() {
        return sumComment;
    }

    public void setSumComment(String sumComment) {
        this.sumComment = sumComment;
    }

    public String getSumLike() {
        return sumLike;
    }

    public void setSumLike(String sumLike) {
        this.sumLike = sumLike;
    }

    public String getSumRead() {
        return sumRead;
    }

    public void setSumRead(String sumRead) {
        this.sumRead = sumRead;
    }

    public String getSumShare() {
        return sumShare;
    }

    public void setSumShare(String sumShare) {
        this.sumShare = sumShare;
    }

    public String getSumUnInterested() {
        return sumUnInterested;
    }

    public void setSumUnInterested(String sumUnInterested) {
        this.sumUnInterested = sumUnInterested;
    }

    public String getSumUnLike() {
        return sumUnLike;
    }

    public void setSumUnLike(String sumUnLike) {
        this.sumUnLike = sumUnLike;
    }
}
