<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">详情</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/log/doEdit.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <input type="hidden" value="${log.id}" name="id"/>
                    <div class="form-group mno">
                        <label for="operateModule" class="col-sm-2 control-label" style="text-align:left;">操作模块</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.operateModule!}" name="operateModule" id="operateModule" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="logContent" class="col-sm-2 control-label" style="text-align:left;">操作内容</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.logContent!}" name="logContent" id="logContent" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="logLevel" class="col-sm-2 control-label" style="text-align:left;">日志级别</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.logLevel!}" name="logLevel" id="logLevel" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="operateTime" class="col-sm-2 control-label" style="text-align:left;">操作时间</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.operateTime!}" name="operateTime" id="operateTime" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="operateType" class="col-sm-2 control-label" style="text-align:left;">操作类型</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.operateType!}" name="operateType" id="operateType" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="operateIp" class="col-sm-2 control-label" style="text-align:left;">操作人ip</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.operateIp!}" name="operateIp" id="operateIp" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="userId" class="col-sm-2 control-label" style="text-align:left;">操作人用户ID</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.userId!}" name="userId" id="userId" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="operateStatus" class="col-sm-2 control-label" style="text-align:left;">operate_status</label>
                        <div class="col-sm-8">
                            <input type="text" value="${log.operateStatus!}" name="operateStatus" id="operateStatus" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/log/list.do');">返回</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>
