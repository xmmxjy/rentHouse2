package com.xmmxjy.hbase;

/**
 * Created by xmm on 2017/3/3.
 */
public class Constants {

    public static String AGE_0 = "0";

    public static String AGE_18 = "18";

    public static String AGE_30 = "30";

    public static String AGE_40 = "40";

    public static String AGE_50 = "50";

    public static String AGE_60 = "60";

    public static String AGE_MAX = "150";
}
