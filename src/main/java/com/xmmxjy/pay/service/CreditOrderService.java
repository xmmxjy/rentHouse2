package com.xmmxjy.pay.service;

import com.xmmxjy.common.service.BaseService;
import com.xmmxjy.pay.entity.CreditOrderEntity;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/
public interface CreditOrderService extends BaseService<CreditOrderEntity>{


    List<CreditOrderEntity> selectByUserId(Integer appuserId);

    Long saveOrder(CreditOrderEntity creditOrderEntity);
}
