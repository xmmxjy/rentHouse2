package com.xmmxjy.system.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.system.dao.LogDao;
import com.xmmxjy.system.entity.LogEntity;
import com.xmmxjy.system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/

@Service("logService")
public class LogServiceImpl extends BaseServiceImpl<LogEntity> implements LogService {
    @Autowired
    private LogDao logDao;

}
