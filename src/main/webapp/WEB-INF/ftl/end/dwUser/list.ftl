<!DOCTYPE html>
<html >
<#include "/end/include/head.ftl"/>
<link href="${basePath}/plug-in-ui/treetable/default/treeTable.css" rel="stylesheet" type="text/css" />
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <form role="form" class="form-inline" action="${basePath}/user/list.do}" method="post"  id="formSubmit">
            <div  class="col-md-10" style="width:100%">
                <div class="panel panel-default">
                    <div class="panel-heading">列表</div>
                    <div class="panel-body">
                        <input name="pageNo" id="pageNo" value="${pageNo}" type="hidden">
                        <input name="pageSize" id="pageSize" th:value="${pageSize}" type="hidden">
                        <div class="search">
                            <div class="form-group col-sm-3">
                                <label for="userId" class="control-label col-sm-3 line34">用户ID</label>
                                <div class="col-sm-8">
                                    <input type="text" name="userId" id="userId" value="${query.userId!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="username" class="control-label col-sm-3 line34">用户名</label>
                                <div class="col-sm-8">
                                    <input type="text" name="username" id="username" value="${query.username!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="email" class="control-label col-sm-3 line34">邮箱</label>
                                <div class="col-sm-8">
                                    <input type="text" name="email" id="email" value="${query.email!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="birthday" class="control-label col-sm-3 line34">生日</label>
                                <div class="col-sm-8">
                                    <input type="text" name="birthday" id="birthday" value="${query.birthday!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="sex" class="control-label col-sm-3 line34">性别</label>
                                <div class="col-sm-8">
                                    <input type="text" name="sex" id="sex" value="${query.sex!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="mobilePhone" class="control-label col-sm-3 line34">手机号码</label>
                                <div class="col-sm-8">
                                    <input type="text" name="mobilePhone" id="mobilePhone" value="${query.mobilePhone!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="currentRegion" class="control-label col-sm-3 line34">所在区域</label>
                                <div class="col-sm-8">
                                    <input type="text" name="currentRegion" id="currentRegion" value="${query.currentRegion!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="homeRegion" class="control-label col-sm-3 line34">家乡</label>
                                <div class="col-sm-8">
                                    <input type="text" name="homeRegion" id="homeRegion" value="${query.homeRegion!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="school" class="control-label col-sm-3 line34">所在学校</label>
                                <div class="col-sm-8">
                                    <input type="text" name="school" id="school" value="${query.school!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="isMarry" class="control-label col-sm-3 line34">婚恋</label>
                                <div class="col-sm-8">
                                    <input type="text" name="isMarry" id="isMarry" value="${query.isMarry!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="constellation" class="control-label col-sm-3 line34">星座</label>
                                <div class="col-sm-8">
                                    <input type="text" name="constellation" id="constellation" value="${query.constellation!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="occupation" class="control-label col-sm-3 line34">职业</label>
                                <div class="col-sm-8">
                                    <input type="text" name="occupation" id="occupation" value="${query.occupation!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="interest" class="control-label col-sm-3 line34">兴趣爱好</label>
                                <div class="col-sm-8">
                                    <input type="text" name="interest" id="interest" value="${query.interest!}" class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">搜  索</button>
                            <div class="clearfix"></div>
                        </div>

                        <div id="legend">
                            <legend  class="le">
                            <@shiro.hasPermission name="dw.user.add"><button type="button" class="btn btn-primary" onclick="javascript:doUrl('${basePath}/user/toAdd.do');" >新增</button></@shiro.hasPermission>
                            </legend>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <th>用户ID</th>
                                <th>用户名</th>
                                <th>邮箱</th>
                                <th>生日</th>
                                <th>性别</th>
                                <th>手机号码</th>
                                <th>所在区域</th>
                                <th>家乡</th>
                                <th>所在学校</th>
                                <th>婚恋</th>
                                <th>星座</th>
                                <th>职业</th>
                                <th>兴趣爱好</th>
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <#list list as info>
                            <tr>
                                <td>${info.userId!}</td>
                                <td>${info.username!}</td>
                                <td>${info.email!}</td>
                                <td>${info.birthday!}</td>
                                <td>${info.sex!}</td>
                                <td>${info.mobilePhone!}</td>
                                <td>${info.currentRegion!}</td>
                                <td>${info.homeRegion!}</td>
                                <td>${info.school!}</td>
                                <td>${info.isMarry!}</td>
                                <td>${info.constellation!}</td>
                                <td>${info.occupation!}</td>
                                <td>${info.interest!}</td>
                                <td class="last">
                                <@shiro.hasPermission name="dw.user.edit"><a href="javascript:void(0)" onclick="javascript:doUrl('${basePath}/user/toEdit.do?id=${info.id}')" >编辑</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="dw.user.delete">  <a onclick="javascript:delData('${basePath}/user/doDelete.do?id=${info.id}')">删除</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="dw.user.detail">	<a onclick="javascript:doUrl('${basePath}/user/toDetail.do?id=${info.id}')">详情</a></@shiro.hasPermission>
                                </td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <ul class="pagination" id="pagination1"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath}/plug-in-ui/treetable/jquery.treeTable.min.js"></script>

<script type="text/javascript">
    //当前页码
    var pageNo = ${pageNo};
    //当前页数
    var pages = ${pages};

    var visiblePages = pages;
    if (pages >= 10) {
        visiblePages = 10;
    }

    $.jqPaginator('#pagination1', {
        totalPages: pages,
        visiblePages: visiblePages,
        currentPage: pageNo,
        prev: '<li class="prev"><a href="javascript:;">上一页</a></li>',
        next: '<li class="next"><a href="javascript:;">下一页</a></li>',
        page: '<li class="page"><a href="javascript:;">{{page}}</a></li>',
        first: '<li class="next"><a href="javascript:;">首页</a></li>',
        last: '<li class="next"><a href="javascript:;">末页</a></li>',
        onPageChange: function (num, type) {
            console.log("num : " + num);
            console.log("type : " + type);
            if (type != "init") {
                //$('#p1').text(type + '：' + num);
                document.getElementById('pageNo').value = num;
                document.getElementById('formSubmit').submit();
            }
        }
    });
    $(function(){
        var option = {
            theme:'default',
            expandLevel : 3,
            beforeExpand : function($treeTable, id) {
            },
            onSelect : function($treeTable, id) {
                window.console && console.log('onSelect:' + id);
            }

        };
        $('#treeTable1').treeTable(option);
    });
</script>
</html>