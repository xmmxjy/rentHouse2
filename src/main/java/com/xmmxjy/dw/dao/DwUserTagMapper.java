package com.xmmxjy.dw.dao;

import com.xmmxjy.dw.entity.DwUserTag;
import tk.mybatis.mapper.common.Mapper;

public interface DwUserTagMapper extends Mapper<DwUserTag> {
}