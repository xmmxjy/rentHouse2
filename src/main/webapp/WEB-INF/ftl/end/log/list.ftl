<!DOCTYPE html>
<html >
<#include "/end/include/head.ftl"/>
<link href="${basePath}/plug-in-ui/treetable/default/treeTable.css" rel="stylesheet" type="text/css" />
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <form role="form" class="form-inline" action="${basePath}/log/list.do" method="post"  id="formSubmit">
            <div  class="col-md-10" style="width:100%">
                <div class="panel panel-default">
                    <div class="panel-heading">列表</div>
                    <div class="panel-body">
                        <input name="pageNo" id="pageNo" value="${pageNo}" type="hidden">
                        <input name="pageSize" id="pageSize" th:value="${pageSize}" type="hidden">
                        <div class="search">
                            <div class="form-group col-sm-3">
                                <label for="operateModule" class="control-label col-sm-3 line34">操作模块</label>
                                <div class="col-sm-8">
                                    <input type="text" name="operateModule" id="operateModule" value="${query.operateModule!}" class="form-control">
                                </div>
                            </div>
                            <#--<div class="form-group col-sm-3">
                                <label for="logContent" class="control-label col-sm-3 line34">操作内容</label>
                                <div class="col-sm-8">
                                    <input type="text" name="logContent" id="logContent" value="${query.logContent!}" class="form-control">
                                </div>
                            </div>-->
                            <div class="form-group col-sm-3">
                                <label for="logLevel" class="control-label col-sm-3 line34">日志级别</label>
                                <div class="col-sm-8">
                                    <input type="text" name="logLevel" id="logLevel" value="${query.logLevel!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="operateTime" class="control-label col-sm-3 line34">操作时间</label>
                                <div class="col-sm-8">
                                    <input type="text" name="operateTime" id="operateTime" value="<#if query.operateTime??>${query.operateTime?string("yyyy-MM-dd HH:mm:ss")}</#if>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="operateType" class="control-label col-sm-3 line34">操作类型</label>
                                <div class="col-sm-8">
                                    <input type="text" name="operateType" id="operateType" value="${query.operateType!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="operateIp" class="control-label col-sm-3 line34">操作人ip</label>
                                <div class="col-sm-8">
                                    <input type="text" name="operateIp" id="operateIp" value="${query.operateIp!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="userId" class="control-label col-sm-3 line34">操作人用户ID</label>
                                <div class="col-sm-8">
                                    <input type="text" name="userId" id="userId" value="${query.userId!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="operateStatus" class="control-label col-sm-3 line34">operate_status</label>
                                <div class="col-sm-8">
                                    <input type="text" name="operateStatus" id="operateStatus" value="${query.operateStatus!}" class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">搜  索</button>
                            <div class="clearfix"></div>
                        </div>

                        <div id="legend">
                            <legend  class="le">
                            <@shiro.hasPermission name="system.log.add"><button type="button" class="btn btn-primary" onclick="javascript:doUrl('${basePath}/log/toAdd.do');" >新增</button></@shiro.hasPermission>
                            </legend>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <th>事件类型</th>
                                <th>操作模块</th>
                                <th>日志级别</th>
                                <th>操作时间</th>
                                <th>操作类型</th>
                                <th>操作人ip</th>
                                <th>操作人用户ID</th>
                                <th>operate_status</th>
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <#list list as info>
                            <tr>
                                <td>${info.operateModule!}</td>
                                <td>${info.logLevel!}</td>
                                <td><#if info.operateTime??>${info.operateTime?string("yyyy-MM-dd HH:mm:ss")}</#if></td>
                                <td>${info.operateType!}</td>
                                <td>${info.operateIp!}</td>
                                <td>${info.userId!}</td>
                                <td>${info.operateStatus!}</td>
                                <td class="last">
                                <@shiro.hasPermission name="system.log.edit"><a href="javascript:void(0)" onclick="javascript:doUrl('${basePath}/log/toEdit.do?id=${info.id}')" >编辑</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="system.log.delete">  <a onclick="javascript:delData('${basePath}/log/doDelete.do?id=${info.id}')">删除</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="system.log.detail">	<a onclick="javascript:doUrl('${basePath}/log/toDetail.do?id=${info.id}')">详情</a></@shiro.hasPermission>
                                </td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <ul class="pagination" id="pagination1"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath}/plug-in-ui/treetable/jquery.treeTable.min.js"></script>

<script type="text/javascript">
    //当前页码
    var pageNo = ${pageNo};
    //当前页数
    var pages = ${pages};

    var visiblePages = pages;
    if (pages >= 10) {
        visiblePages = 10;
    }

    $.jqPaginator('#pagination1', {
        totalPages: pages,
        visiblePages: visiblePages,
        currentPage: pageNo,
        prev: '<li class="prev"><a href="javascript:;">上一页</a></li>',
        next: '<li class="next"><a href="javascript:;">下一页</a></li>',
        page: '<li class="page"><a href="javascript:;">{{page}}</a></li>',
        first: '<li class="next"><a href="javascript:;">首页</a></li>',
        last: '<li class="next"><a href="javascript:;">末页</a></li>',
        onPageChange: function (num, type) {
            console.log("num : " + num);
            console.log("type : " + type);
            if (type != "init") {
                //$('#p1').text(type + '：' + num);
                document.getElementById('pageNo').value = num;
                document.getElementById('formSubmit').submit();
            }
        }
    });
</script>
</html>