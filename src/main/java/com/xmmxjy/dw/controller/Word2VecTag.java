package com.xmmxjy.dw.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


import com.hankcs.hanlp.HanLP;

import com.xmmxjy.dw.entity.Tag;
import com.xmmxjy.dw.util.MinHeapFloat;
import me.xiaosheng.word2vec.Word2Vec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Word2VecTag {
	private static Logger logger = LoggerFactory.getLogger(Word2VecTag.class);

	public static void main(String[] args) {
		String xx = "1,深圳地铁将设立VIP头等车厢买双倍票可享坐票,社会,深圳 地铁 头等舱,深圳 地铁 头等舱,南都讯　记者刘凡　周昌和　任笑一　继推出日票后，深圳今后将设地铁ＶＩＰ头等车厢，设坐票制。昨日，《南都ＭＥＴＲＯ》创刊仪式暨２０１２年深港地铁圈高峰论坛上透露，在未来的１１号线上将增加特色服务，满足不同消费层次的乘客的不同需求，如特设行李架的车厢和买双倍票可有座位坐的ＶＩＰ车厢等。论坛上，深圳市政府副秘书长、轨道交通建设办公室主任赵鹏林透露，地铁未来的方向将分等级，满足不同层次的人的需求，提供不同层次的有针对的服务。其中包括一些档次稍微高一些的服务。“我们要让公共交通也能满足档次稍高一些的服务”。比如，尝试有座位的地铁票服务。尤其是一些远道而来的乘客，通过提供坐票服务，让乘坐地铁也能享受到非常舒适的体验。他说，这种坐票的服务有望在地铁３期上实行，将加挂２节车厢以实施花钱可买座位的服务。“我们希望轨道交通和家里开的车一样，分很多种。”赵鹏林说，比如有些地铁是“观光线”，不仅沿途的风光非常好，还能凭一张票无数次上下，如同旅游时提供的“通票服务”。再比如，设立可以放大件行李的车厢，今后通过设专门可放大件行李的座位，避免像现在放行李不太方便的现象。“未来地铁初步不仅在干线上铺设，还会在支线、城际线上去建设。”“觉得如果车费不太贵的话，还是愿意考虑的。”昨日市民黄小姐表示，尤其是从老街到机场这一段，老街站每次上下客都很多人，而如果赶上上下班高峰期，特别拥挤，要一路从老街站站到机场，４０、５０分钟还是挺吃力的，宁愿多花点钱也能稍微舒适一点。但是白领林先生则表示，自己每天上下班都要坐地铁，出双倍车资买坐票费用有点高";
		Word2Vec vec = new Word2Vec();
		try {
			//加载google模型
			vec.loadGoogleModel("D:/eclipseWork/WordCount/src/data/wiki_chinese_word2vec(Google).model");
		} catch (IOException e) {
		    e.printStackTrace();
		}
        StringTokenizer tokenizerLine = new StringTokenizer(xx);
        Long id = Long.parseLong(tokenizerLine.nextToken(","));// 新闻ID
        String title = tokenizerLine.nextToken(",");
        logger.info("title : " + title);
        String channel = tokenizerLine.nextToken(",");
        logger.info("channel : " + channel);
        String abstractStr = tokenizerLine.nextToken(",");
        logger.info("abstractStr : " + abstractStr);
        String keyWords = tokenizerLine.nextToken(",");
        logger.info("keyWords : " + keyWords);
        String content = tokenizerLine.nextToken(",");// 新闻内容部分
        logger.info("content : " + content);
        
        //提取关键词
        List<String> words = HanLP.extractKeyword(content,20);
        //加载标签列表
        List<String> tagList = readTxtFile("D:/eclipseWork/WordCount/src/tag.txt");
        Set<Tag> tags = new HashSet<Tag>();
        for (String tag : tagList) {
			for (String word : words) {
				float f = vec.wordSimilarity(tag, word);
				//logger.info(tag + "|" + word + f);
				Tag m = new Tag(tag,f);
				tags.add(m);
			}
		}
        int k=10;

        MinHeapFloat heap = new MinHeapFloat(k);
        for(Tag i:tags){
            heap.add(i);
        }

        logger.info(heap.toString());
		//System.out.println("狗|猫: " + vec.wordSimilarity("狗", "猫"));
	}
	
	public static List<String> readTxtFile(String filePath){
		List<String> tagList = new ArrayList<String>();
        try {
                String encoding="utf-8";
                File file=new File(filePath);
                if(file.isFile() && file.exists()){ //判断文件是否存在
                    InputStreamReader read = new InputStreamReader(
                    new FileInputStream(file),encoding);//考虑到编码格式
                    BufferedReader bufferedReader = new BufferedReader(read);
                    String lineTxt = null;
                    while((lineTxt = bufferedReader.readLine()) != null){
                        StringTokenizer tokenizerLine = new StringTokenizer(lineTxt);
                        Long id = Long.parseLong(tokenizerLine.nextToken(","));// 新闻ID
                        String title = tokenizerLine.nextToken(",");
                        logger.info("title : " + title);
                        tagList.add(title);
                    }
                    read.close();
        }else{
            System.out.println("找不到指定的文件");
        }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
     return tagList;
    }
}
