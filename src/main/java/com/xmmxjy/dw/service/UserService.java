package com.xmmxjy.dw.service;

import com.xmmxjy.common.service.BaseService;
import com.xmmxjy.dw.entity.DwUserTag;
import com.xmmxjy.dw.entity.UserEntity;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/
public interface UserService extends BaseService<UserEntity>{


    DwUserTag selectTag(String userId, long tagId);
}
