package com.xmmxjy.dw.dao;

import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.dw.entity.ArticleEntity;
import tk.mybatis.mapper.common.Mapper;

public interface ArticleDao extends MyMapper<ArticleEntity> {
}