package com.xmmxjy.dw.entity;

import javax.persistence.*;

@Table(name = "dw_news_tag")
public class DwNewsTag {
    /**
     * 标签ID
     */
    @Id
    @Column(name = "tag_id")
    private Long tagId;

    /**
     * 新闻ID
     */
    @Id
    @Column(name = "news_id")
    private Long newsId;

    /**
     * 权重
     */
    private Double weight;

    /**
     * 获取标签ID
     *
     * @return tag_id - 标签ID
     */
    public Long getTagId() {
        return tagId;
    }

    /**
     * 设置标签ID
     *
     * @param tagId 标签ID
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    /**
     * 获取新闻ID
     *
     * @return news_id - 新闻ID
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * 设置新闻ID
     *
     * @param newsId 新闻ID
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * 获取权重
     *
     * @return weight - 权重
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * 设置权重
     *
     * @param weight 权重
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }
}