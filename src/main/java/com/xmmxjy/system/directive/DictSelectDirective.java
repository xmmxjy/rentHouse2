package com.xmmxjy.system.directive;

import com.xmmxjy.common.util.SpringUtils;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.system.entity.DictDataEntity;
import com.xmmxjy.system.service.DictDataService;
import com.xmmxjy.system.service.DictTypeService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.apache.hadoop.hdfs.security.token.block.DataEncryptionKey;
import org.aspectj.weaver.tools.cache.SimpleCache;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

/**
 * Created by xmm on 2017/2/24.
 */
@Component(value = "dictSelect")
public class DictSelectDirective implements TemplateDirectiveModel{


    /**
     * 数据字典 下拉列表
     * 用法：<@dictSelect id="select" name="" class="" code="sex" value="1" option="请选择" showSelect="true"/>
     * @param environment
     * @param map
     * @param templateModels
     * @param templateDirectiveBody
     * @throws TemplateException
     * @throws IOException
     */
    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        SimpleScalar id = (SimpleScalar)map.get("id"); //select id
        SimpleScalar name = (SimpleScalar)map.get("name");//select name
        SimpleScalar code = (SimpleScalar)map.get("code");//数据字典类型
        SimpleScalar clazz = (SimpleScalar)map.get("class");//select class 样式
        SimpleScalar value = (SimpleScalar)map.get("value");//数据字典的值
        SimpleScalar showSelect = (SimpleScalar) map.get("showSelect"); // 是否显示请选择option
        SimpleScalar option = (SimpleScalar)map.get("option");//option默认显示的数据

        Writer out = environment.getOut();

        if (code != null && Tools.notEmpty(code.getAsString())) {
            StringBuilder builder = new StringBuilder();
            builder.append("<select ");
            if (id != null && Tools.notEmpty(id.getAsString())) {
                builder.append("id = '").append(id).append("' ");
            }
            if (name != null && Tools.notEmpty(name.getAsString())) {
                builder.append("name ='").append(name).append("' ");
            }
            if (clazz != null && Tools.notEmpty(clazz.getAsString())) {
                builder.append("class ='").append(clazz).append("' ");
            }
            builder.append(">");
            if (showSelect == null || !showSelect.getAsString().equals("false")) {
                builder.append("<option value='0'>");
                // 如果默认显示值不为空，即需要显示特定的文本，则加入option值
                if(option != null && Tools.notEmpty(option.getAsString())) {
                    builder.append(option);
                } else {
                    builder.append("请选择");
                }
                builder.append("</option>");
            }
            //
            DictDataService dictDataService = (DictDataService) SpringUtils.getBean("dictDataService");

            List<DictDataEntity> optionList = dictDataService.selectByCode(code.getAsString());
            if (optionList != null && optionList.size() > 0) {
                for (int i = 0; i < optionList.size();i++) {
                    DictDataEntity dictData = optionList.get(i);
                    String dName = dictData.getName();
                    String dValue = dictData.getValue();
                    builder.append("<option value='").append(dValue).append("' ");
                    if (value != null) {
                        if (Tools.notEmpty(value.getAsString())) {
                            if (dValue.equals(value.getAsString())) {
                                builder.append("selected");
                            }
                        }
                    }
                    builder.append(">").append(dName).append("</option>");
                }
            }
            builder.append("</select>");
            out.write(builder.toString());
        }

    }
}
