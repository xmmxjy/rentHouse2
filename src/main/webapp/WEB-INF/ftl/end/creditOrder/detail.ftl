<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">详情</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/creditOrder/doEdit.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <input type="hidden" value="${creditOrder.id}" name="id"/>
                    <div class="form-group mno">
                        <label for="creditNum" class="col-sm-2 control-label" style="text-align:left;">积分数值</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.creditNum!}" name="creditNum" id="creditNum" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="money" class="col-sm-2 control-label" style="text-align:left;">金额</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.money!}" name="money" id="money" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="subject" class="col-sm-2 control-label" style="text-align:left;">商品名称</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.subject!}" name="subject" id="subject" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="subjectDetail" class="col-sm-2 control-label" style="text-align:left;">商品详情</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.subjectDetail!}" name="subjectDetail" id="subjectDetail" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="payType" class="col-sm-2 control-label" style="text-align:left;">支付方式</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.payType!}" name="payType" id="payType" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="payOs" class="col-sm-2 control-label" style="text-align:left;">支付的操作系统</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.payOs!}" name="payOs" id="payOs" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="orderStatus" class="col-sm-2 control-label" style="text-align:left;">订单状态</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.orderStatus!}" name="orderStatus" id="orderStatus" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="orderTime" class="col-sm-2 control-label" style="text-align:left;">下单时间</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.orderTime!}" name="orderTime" id="orderTime" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="payTime" class="col-sm-2 control-label" style="text-align:left;">支付时间</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.payTime!}" name="payTime" id="payTime" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="appuserId" class="col-sm-2 control-label" style="text-align:left;">用户ID</label>
                        <div class="col-sm-8">
                            <input type="text" value="${creditOrder.appuserId!}" name="appuserId" id="appuserId" class="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group mno">
                    <div class="col-sm-offset-1 col-sm-6">
                        <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/creditOrder/list.do');">返回</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>
