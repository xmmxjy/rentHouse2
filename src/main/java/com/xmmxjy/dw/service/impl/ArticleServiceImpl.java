package com.xmmxjy.dw.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.dw.dao.ArticleDao;
import com.xmmxjy.dw.entity.ArticleEntity;
import com.xmmxjy.dw.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author: xmm
* @version:1.0
*/

@Service("articleService")
public class ArticleServiceImpl extends BaseServiceImpl<ArticleEntity> implements ArticleService {
    @Autowired
    private ArticleDao articleDao;

}
