package com.xmmxjy.hbase.entity;

import java.math.BigDecimal;

/**
 * Created by xmm on 2017/2/16.
 */
public class DwTopic implements Comparable<DwTopic>{
    private String topicName;
    private double topicWeight;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public double getTopicWeight() {
        return topicWeight;
    }

    public void setTopicWeight(double topicWeight) {
        this.topicWeight = topicWeight;
    }

    @Override
    public String toString() {
        return "{" +
                "topicName='" + topicName + '\'' +
                ", topicWeight=" + topicWeight +
                '}';
    }

    @Override
    public int compareTo(DwTopic o) {
        return new BigDecimal(o.getTopicWeight()).compareTo(new BigDecimal(this.getTopicWeight()));
    }
}
