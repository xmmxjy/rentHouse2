<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">用户画像展示</div>
            <div class="panel-body">
                <div id="user" style="width: 800px;height:600px;margin: auto"></div>
                <div id="topic" style="width: 800px;height:600px;margin: auto"></div>
                <div id="category" style="width: 800px;height:600px;margin: auto"></div>
                <div id="channel" style="width: 800px;height:600px;margin: auto"></div>
                <div id="sex" style="width: 800px;height:600px;margin: auto"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript" src="${basePath}/plug-in-ui/echarts/echarts.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/echarts/echarts-wordcloud.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>
<script type="text/javascript">
    var topics = ${topics!};
    var topicData = new Array();
    if (topics != null && topics != "") {
        $.each(topics, function (i, topic) {
            var t = {
                name: topic['topicName'],
                value: topic['topicWeight']
            }
            topicData.push(t);
        });
    }

    var categorys = ${categorys!};
    var categoryData = new Array();
    if (categorys != null && categorys != "") {
        $.each(categorys, function (i, category) {
            var t = {
                name: category['categoryCode'],
                value: category['categroyWeight']
            }
            categoryData.push(t);
        });
    }

    // 基于准备好的dom，初始化echarts实例
    var topicChart = echarts.init(document.getElementById('topic'));
    var categoryChart = echarts.init(document.getElementById('category'));

    // 指定图表的配置项和数据
    if (topicData.length > 0) {
        var topicOption = {
            title: {
                text: '用户的主题词偏好',
                subtext: '',
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    name: '主题词权重',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: topicData,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        topicChart.setOption(topicOption);
    } else {
        $("#topic").css("display","none");
    }
    if (categoryData.length > 0) {
        var categoryOption = {
            title: {
                text: '用户的分类偏好',
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series: [
                {
                    name: '分类权重',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: categoryData,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        categoryChart.setOption(categoryOption);
    } else {
        $("#category").css("display","none");
    }


    //用户的频道偏好
    var channels = ${channels!};
    var channelData = new Array();
    if (channels != "" && channels != null) {
        $.each(channels, function (i, channel) {
            var t = {
                name: channel['channelName'],
                value: channel['channelWeight']
            }
            channelData.push(t);
        });
    }

    if (channelData.length > 0) {
        var channelChart = echarts.init(document.getElementById('channel'));

        // 指定图表的配置项和数据
        var channelOption = {
            title : {
                text: '用户的频道偏好',
                subtext: '',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            series : [
                {
                    name: '频道权重',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:channelData,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        channelChart.setOption(channelOption);
    } else {
        $("#channel").css("display","none");
    }

    //用户的基本信息展示
    var user = ${user!};
    console.log(user);
    var userData = new Array();
    for (var u in user) {
        var t = {
            name : user[u],
            value : 100,
            key : u
        }
        userData.push(t);
    }
    for (var i = 0; i < 20;i++) {
        var t = {
            name : 'name' + i,
            value : 100,
            key : 'key' + i
        }
        userData.push(t);
    }
    console.log(userData);
    var schema = [
        {name: 'name', index: 0},
        {name: 'value', index: 1},
        {name: 'key', index: 2}
    ];
    var maskImage = new Image();

    var userOption = {
        title:{
            text:"用户基本信息",
            subtext: '',
            x:'center'
        },
        tooltip: {
            padding: 10,
/*            backgroundColor: '#14221b',
            borderColor: '#777',*/
            borderWidth: 1,
            formatter: function (obj) {
                var value = obj.value;
                return '<div style="font-size: 18px;padding-bottom: 7px;">'
                        + obj.data.key + "</br>" + obj.data.name
                        + '</div>'
            }
        },
        series: [{
            type: 'wordCloud',
            gridSize: 2,
            sizeRange: [10, 100],
            rotationRange: [-90, 90],
            rotationStep: 45,
            shape: 'pentagon',
            maskImage: maskImage,
            textStyle: {
                normal: {
                    color: function() {
                        return 'rgb(' + [
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160),
                                    Math.round(Math.random() * 160)
                                ].join(',') + ')';
                    }
                },
                emphasis: {
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            data: userData
        }]
    };
    var userChart = echarts.init(document.getElementById('user'));
    maskImage.onload = function () {
        userOption.series[0].maskImage
        userChart.setOption(userOption);
    }

    maskImage.src = '${basePath}/plug-in-ui/echarts/logo.png';


    //性别分布

    var sex = ${sex!};
    var sexData = new Array();
    console.log(sex);
    if (sex != "") {

    var sexChart = echarts.init(document.getElementById('sex'));

    var sexOption = {
        title : {
          text : "性别分布"
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            x: 'left',
            data:['男','女','未知']
        },
        series: [
            {
                name:'性别分布',
                type:'pie',
                radius: ['50%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '30',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data:sex
            }
        ]
    };
    sexChart.setOption(sexOption);
    } else {
        $("#sex").css("display","none");
    }


</script>