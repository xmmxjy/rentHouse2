package com.xmmxjy.dw.service;

import com.xmmxjy.common.service.BaseService;
import com.xmmxjy.dw.entity.ArticleEntity;

/**
* @author: xmm
* @version:1.0
*/
public interface ArticleService extends BaseService<ArticleEntity>{


}
