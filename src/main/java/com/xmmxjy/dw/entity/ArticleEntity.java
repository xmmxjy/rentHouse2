package com.xmmxjy.dw.entity;

import java.util.Date;
import javax.persistence.*;
@Table(name = "article")
public class ArticleEntity {
    @Id
    private String id;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "modify_date")
    private Date modifyDate;

    private String author;

    private Integer hits;

    @Column(name = "is_publication")
    private Boolean isPublication;

    @Column(name = "is_recommend")
    private Boolean isRecommend;

    @Column(name = "is_top")
    private Boolean isTop;

    private String title;

    private String url;

    private String keyword;

    @Column(name = "article_category_id")
    private String articleCategoryId;

    @Column(name = "is_audit")
    private Byte isAudit;

    @Column(name = "comment_count")
    private Long commentCount;

    @Column(name = "collect_count")
    private Long collectCount;

    private String source;

    private String publishtime;

    private String content;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return modify_date
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * @param modifyDate
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return hits
     */
    public Integer getHits() {
        return hits;
    }

    /**
     * @param hits
     */
    public void setHits(Integer hits) {
        this.hits = hits;
    }

    /**
     * @return is_publication
     */
    public Boolean getIsPublication() {
        return isPublication;
    }

    /**
     * @param isPublication
     */
    public void setIsPublication(Boolean isPublication) {
        this.isPublication = isPublication;
    }

    /**
     * @return is_recommend
     */
    public Boolean getIsRecommend() {
        return isRecommend;
    }

    /**
     * @param isRecommend
     */
    public void setIsRecommend(Boolean isRecommend) {
        this.isRecommend = isRecommend;
    }

    /**
     * @return is_top
     */
    public Boolean getIsTop() {
        return isTop;
    }

    /**
     * @param isTop
     */
    public void setIsTop(Boolean isTop) {
        this.isTop = isTop;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * @param keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * @return article_category_id
     */
    public String getArticleCategoryId() {
        return articleCategoryId;
    }

    /**
     * @param articleCategoryId
     */
    public void setArticleCategoryId(String articleCategoryId) {
        this.articleCategoryId = articleCategoryId;
    }

    /**
     * @return is_audit
     */
    public Byte getIsAudit() {
        return isAudit;
    }

    /**
     * @param isAudit
     */
    public void setIsAudit(Byte isAudit) {
        this.isAudit = isAudit;
    }

    /**
     * @return comment_count
     */
    public Long getCommentCount() {
        return commentCount;
    }

    /**
     * @param commentCount
     */
    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * @return collect_count
     */
    public Long getCollectCount() {
        return collectCount;
    }

    /**
     * @param collectCount
     */
    public void setCollectCount(Long collectCount) {
        this.collectCount = collectCount;
    }

    /**
     * @return source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return publishtime
     */
    public String getPublishtime() {
        return publishtime;
    }

    /**
     * @param publishtime
     */
    public void setPublishtime(String publishtime) {
        this.publishtime = publishtime;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }
}