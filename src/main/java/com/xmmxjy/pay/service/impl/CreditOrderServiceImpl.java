package com.xmmxjy.pay.service.impl;

import com.xmmxjy.common.service.impl.BaseServiceImpl;
import com.xmmxjy.pay.dao.CreditOrderDao;
import com.xmmxjy.pay.entity.CreditOrderEntity;
import com.xmmxjy.pay.service.CreditOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/

@Service("creditOrderService")
public class CreditOrderServiceImpl extends BaseServiceImpl<CreditOrderEntity> implements CreditOrderService {
    @Autowired
    private CreditOrderDao creditOrderDao;

    @Override
    public List<CreditOrderEntity> selectByUserId(Integer appuserId) {
        return creditOrderDao.selectByUserId(appuserId);
    }

    @Override
    public Long saveOrder(CreditOrderEntity creditOrderEntity) {
        int i= creditOrderDao.insertUseGeneratedKeys(creditOrderEntity);
        if (i > 0) {
            return creditOrderEntity.getId();
        }
        return null;
    }
}
