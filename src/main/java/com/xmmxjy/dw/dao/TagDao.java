package com.xmmxjy.dw.dao;


import com.xmmxjy.common.mapper.MyMapper;
import com.xmmxjy.dw.entity.TagEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author：xmm
* @version:1.0
*/
public interface TagDao extends MyMapper<TagEntity> {

}

