package com.xmmxjy.dw.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hankcs.hanlp.HanLP;
import com.xmmxjy.common.controller.BaseEndController;
import com.xmmxjy.common.util.AjaxJson;
import com.xmmxjy.common.util.BeanUtil;
import com.xmmxjy.common.util.EndUtil;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.dw.entity.NewsEntity;
import com.xmmxjy.dw.entity.Tag;
import com.xmmxjy.dw.entity.TagEntity;
import com.xmmxjy.dw.service.NewsService;
import com.xmmxjy.dw.util.MinHeapFloat;
import com.xmmxjy.dw.util.TagUtil;
import com.xmmxjy.dw.util.Word2VecUtil;
import me.xiaosheng.word2vec.Word2Vec;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
*
* @author xmm
* @version :1.0
*/
@Controller
@RequestMapping(value="/news")
public class NewsController extends BaseEndController {

private static final Logger logger = LoggerFactory.getLogger(NewsController.class);

private static final String CURRENT_PAGE = "news/";

@Autowired
private NewsService newsService;


/**
* 列表页面
* @return
*/
@RequiresPermissions("dw.news.list")
@RequestMapping(value = "/list.do",method = {RequestMethod.GET,RequestMethod.POST})
public String list(@ModelAttribute NewsEntity query, HttpServletRequest request, HttpServletResponse response,
@RequestParam(required = false, value = "pageNo", defaultValue = "1") int pageNo,
@RequestParam(required = false, value = "pageSize", defaultValue = "10") int pageSize,ModelMap model) throws Exception{
//用了分页组件
    PageHelper.startPage(pageNo, pageSize);
    NewsEntity news = new NewsEntity();
    /**
    * 页面会转成空字符串，这里转成null，待后续想其他办法，这里加上转换，性能肯定有影响了
    */
    BeanUtil.copyBean2Bean(news,query);
List<NewsEntity> list = newsService.select(news);
    logger.info("list : {}",list);
    model.addAttribute("query",query);
    PageInfo page = new PageInfo(list);
    //传入分页所需的参数
    EndUtil.sendPageParams(request,model,page);
    //传入固定的参数
    EndUtil.sendEndParams(request,model);
    return END_PAGE + CURRENT_PAGE + LIST;
    }

    /**
    * 新增页面
    * @param request
    * @param response
    * @param model
    * @return
    */
    @RequiresPermissions("dw.news.add")
    @RequestMapping(value = "/toAdd.do",method ={RequestMethod.GET, RequestMethod.POST})
    public String toAdd(HttpServletRequest request,HttpServletResponse response,ModelMap model){
        EndUtil.sendEndParams(request,model);
        return END_PAGE + CURRENT_PAGE + ADD;
    }

    /**
    * 修改页面
    * @param request
    * @param response
    * @param model
    * @return
    */
    @RequiresPermissions("dw.news.edit")
    @RequestMapping(value = "/toEdit.do",method ={RequestMethod.GET, RequestMethod.POST})
    public String toEdit(@RequestParam(required = true, value = "id" ) Long id,HttpServletRequest request,HttpServletResponse response,ModelMap model){
    EndUtil.sendEndParams(request,model);
    NewsEntity news = newsService.selectByPrimaryKey(id);
        model.addAttribute("news",news);
        return END_PAGE + CURRENT_PAGE + EDIT;
    }

    /**
    * 详情页面
    * @param request
    * @param response
    * @param model
    * @return
    */
    @RequiresPermissions("dw.news.detail")
    @RequestMapping(value = "/toDetail.do",method ={RequestMethod.GET, RequestMethod.POST})
    public String toDetail(@RequestParam(required = true, value = "id" ) Long id,HttpServletRequest request,HttpServletResponse response,ModelMap model){
        EndUtil.sendEndParams(request,model);
        NewsEntity news = newsService.selectByPrimaryKey(id);
        model.addAttribute("news",news);
        return END_PAGE + CURRENT_PAGE + DETAIL;
    }




    /**
    * 保存方法
    * @param news
    * @return
    */
    @RequiresPermissions("dw.news.add")
    @RequestMapping(value = "/doAdd.do",method ={RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public AjaxJson doAdd(@ModelAttribute("news") NewsEntity news){
        AjaxJson j = new AjaxJson();
        try {
            newsService.save(news);
            j.setMsg("保存成功");
        } catch (Exception e) {
            logger.info(e.getMessage());
            j.setSuccess(false);
            j.setMsg("保存失败");
        }
        return j;
    }

    /**
    * 更新方法
    * @param news
    * @return
    */
    @RequiresPermissions("dw.news.edit")
    @RequestMapping(value = "/doEdit.do",method ={RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public AjaxJson doEdit(@ModelAttribute("news") NewsEntity news){
    AjaxJson j = new AjaxJson();
        try {
        int i = newsService.updateByPrimaryKey(news);
        logger.info("i : ---- {}",i);
            if (i > 0) {
                j.setMsg("更新成功");
            } else {
                j.setMsg("更新失败");
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            j.setSuccess(false);
            j.setMsg("更新失败");
        }
        return j;
    }

    @RequiresPermissions("dw.news.delete")
    @RequestMapping(value = "/doDelete.do",method ={RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public AjaxJson doDelete(@RequestParam(required = true, value = "id")Long id){
    AjaxJson j = new AjaxJson();
    try {
        int i= newsService.delete(id);
        if (i > 0) {
            j.setMsg("删除成功");
        } else {
            j.setMsg("删除失败");
        }
    } catch (Exception e) {
        logger.info(e.getMessage());
        j.setSuccess(false);
        j.setMsg("删除失败");
    }
        return j;
    }

    @RequestMapping(value = "/toTag.do",method ={RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public AjaxJson toTag(@RequestParam(required = true, value = "id")Long id){
        AjaxJson j = new AjaxJson();
        try {
            Word2Vec vec = Word2VecUtil.get();
            NewsEntity news = newsService.selectByPrimaryKey(id);
            String content = news.getContent();
            //提取关键词
            List<String> words = HanLP.extractKeyword(content,20);
            List<TagEntity> tagList = TagUtil.getTagList();
            Set<Tag> tags = new HashSet<Tag>();
            for (TagEntity tag : tagList) {
                for (String word : words) {
                    float f = vec.wordSimilarity(tag.getTag(), word);
                    //logger.info(tag + "|" + word + f);
                    Tag m = new Tag(tag.getId(),tag.getTag(),f);
                    tags.add(m);
                }
            }
            int k=10;

            MinHeapFloat heap = new MinHeapFloat(k);
            for(Tag i:tags){
                heap.add(i);
            }
            Tag [] data = heap.getData();
            newsService.saveTag(id,data);
            logger.info(heap.toString());
            j.setMsg("标签化成功");
        } catch (Exception e) {
            logger.info(e.getMessage());
            j.setSuccess(false);
            j.setMsg("标签失败");
        }
        return j;
    }


}
