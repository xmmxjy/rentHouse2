package com.xmmxjy.system.service;

import com.xmmxjy.common.service.BaseService;
import com.xmmxjy.system.entity.DictDataEntity;
import freemarker.template.SimpleScalar;

import java.util.List;

/**
* @author: xmm
* @version:1.0
*/
public interface DictDataService extends BaseService<DictDataEntity>{


    List<DictDataEntity> selectByCode(String code);

    String getDictDataName(String code, String value);
}
