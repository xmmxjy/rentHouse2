<!DOCTYPE html>
<html >
<#include "/end/include/head.ftl"/>
<link href="${basePath}/plug-in-ui/treetable/default/treeTable.css" rel="stylesheet" type="text/css" />
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <form role="form" class="form-inline" action="${basePath}/creditOrder/list.do}" method="post"  id="formSubmit">
            <div  class="col-md-10" style="width:100%">
                <div class="panel panel-default">
                    <div class="panel-heading">列表</div>
                    <div class="panel-body">
                        <input name="pageNo" id="pageNo" value="${pageNo}" type="hidden">
                        <input name="pageSize" id="pageSize" th:value="${pageSize}" type="hidden">
                        <div class="search">
                            <div class="form-group col-sm-3">
                                <label for="creditNum" class="control-label col-sm-3 line34">积分数值</label>
                                <div class="col-sm-8">
                                    <input type="text" name="creditNum" id="creditNum" value="${query.creditNum!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="money" class="control-label col-sm-3 line34">金额</label>
                                <div class="col-sm-8">
                                    <input type="text" name="money" id="money" value="${query.money!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="subject" class="control-label col-sm-3 line34">商品名称</label>
                                <div class="col-sm-8">
                                    <input type="text" name="subject" id="subject" value="${query.subject!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="subjectDetail" class="control-label col-sm-3 line34">商品详情</label>
                                <div class="col-sm-8">
                                    <input type="text" name="subjectDetail" id="subjectDetail" value="${query.subjectDetail!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="payType" class="control-label col-sm-3 line34">支付方式</label>
                                <div class="col-sm-8">
                                    <input type="text" name="payType" id="payType" value="${query.payType!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="payOs" class="control-label col-sm-3 line34">支付的操作系统</label>
                                <div class="col-sm-8">
                                    <input type="text" name="payOs" id="payOs" value="${query.payOs!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="orderStatus" class="control-label col-sm-3 line34">订单状态</label>
                                <div class="col-sm-8">
                                    <input type="text" name="orderStatus" id="orderStatus" value="${query.orderStatus!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="orderTime" class="control-label col-sm-3 line34">下单时间</label>
                                <div class="col-sm-8">
                                    <input type="text" name="orderTime" id="orderTime" value="${query.orderTime!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="payTime" class="control-label col-sm-3 line34">支付时间</label>
                                <div class="col-sm-8">
                                    <input type="text" name="payTime" id="payTime" value="${query.payTime!}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="appuserId" class="control-label col-sm-3 line34">用户ID</label>
                                <div class="col-sm-8">
                                    <input type="text" name="appuserId" id="appuserId" value="${query.appuserId!}" class="form-control">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">搜  索</button>
                            <div class="clearfix"></div>
                        </div>

                        <div id="legend">
                            <legend  class="le">
                            <@shiro.hasPermission name="pay.creditOrder.add"><button type="button" class="btn btn-primary" onclick="javascript:doUrl('${basePath}/creditOrder/toAdd.do');" >新增</button></@shiro.hasPermission>
                            </legend>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <th>积分数值</th>
                                <th>金额</th>
                                <th>商品名称</th>
                                <th>商品详情</th>
                                <th>支付方式</th>
                                <th>支付的操作系统</th>
                                <th>订单状态</th>
                                <th>下单时间</th>
                                <th>支付时间</th>
                                <th>用户ID</th>
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <#list list as info>
                            <tr>
                                <td>${info.creditNum!}</td>
                                <td>${info.money!}</td>
                                <td>${info.subject!}</td>
                                <td>${info.subjectDetail!}</td>
                                <td>${info.payType!}</td>
                                <td>${info.payOs!}</td>
                                <td>${info.orderStatus!}</td>
                                <td>${info.orderTime!}</td>
                                <td>${info.payTime!}</td>
                                <td>${info.appuserId!}</td>
                                <td class="last">
                                <@shiro.hasPermission name="pay.creditOrder.edit"><a href="javascript:void(0)" onclick="javascript:doUrl('${basePath}/creditOrder/toEdit.do?id=${info.id}')" >编辑</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="pay.creditOrder.delete">  <a onclick="javascript:delData('${basePath}/creditOrder/doDelete.do?id=${info.id}')">删除</a></@shiro.hasPermission>
                                <@shiro.hasPermission name="pay.creditOrder.detail">	<a onclick="javascript:doUrl('${basePath}/creditOrder/toDetail.do?id=${info.id}')">详情</a></@shiro.hasPermission>
                                </td>
                            </tr>
                            </#list>
                            </tbody>
                        </table>
                        <div class="text-right">
                            <ul class="pagination" id="pagination1"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script type="text/javascript" src="${basePath}/plug-in-ui/treetable/jquery.treeTable.min.js"></script>

<script type="text/javascript">
    //当前页码
    var pageNo = ${pageNo};
    //当前页数
    var pages = ${pages};

    var visiblePages = pages;
    if (pages >= 10) {
        visiblePages = 10;
    }

    $.jqPaginator('#pagination1', {
        totalPages: pages,
        visiblePages: visiblePages,
        currentPage: pageNo,
        prev: '<li class="prev"><a href="javascript:;">上一页</a></li>',
        next: '<li class="next"><a href="javascript:;">下一页</a></li>',
        page: '<li class="page"><a href="javascript:;">{{page}}</a></li>',
        first: '<li class="next"><a href="javascript:;">首页</a></li>',
        last: '<li class="next"><a href="javascript:;">末页</a></li>',
        onPageChange: function (num, type) {
            console.log("num : " + num);
            console.log("type : " + type);
            if (type != "init") {
                //$('#p1').text(type + '：' + num);
                document.getElementById('pageNo').value = num;
                document.getElementById('formSubmit').submit();
            }
        }
    });
    $(function(){
        var option = {
            theme:'default',
            expandLevel : 3,
            beforeExpand : function($treeTable, id) {
            },
            onSelect : function($treeTable, id) {
                window.console && console.log('onSelect:' + id);
            }

        };
        $('#treeTable1').treeTable(option);
    });
</script>
</html>