package com.xmmxjy.hbase.util;

import com.xmmxjy.common.util.DateUtil;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.hbase.entity.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by xmm on 2017/2/17.
 */
public class BeanUtil {

    public static List<DwCategory> CATEGORYS = null;

    static {
        CATEGORYS = new ArrayList<DwCategory>();
        CATEGORYS.add(new DwCategory("101001","运检"));
        CATEGORYS.add(new DwCategory("101002","农电"));
        CATEGORYS.add(new DwCategory("101003","营销"));
        CATEGORYS.add(new DwCategory("101004","电动汽车"));
        CATEGORYS.add(new DwCategory("101005","电建"));
        CATEGORYS.add(new DwCategory("101006","政工"));
        CATEGORYS.add(new DwCategory("101009","能源"));
        CATEGORYS.add(new DwCategory("101010","科技"));
        CATEGORYS.add(new DwCategory("101012001","文学"));
        CATEGORYS.add(new DwCategory("101012005","生活"));
    }



    public static DwBaseUser pageToEntity(DwUserPage page) {
        DwBaseUser base = new DwBaseUser();
        base.setId(page.getId());
        if (Tools.notEmpty(page.getUsername())) {
            base.setUsername(page.getUsername());
        }
        String birthday = page.getBirthday();
        if (Tools.notEmpty(birthday)) {
            //生日
            base.setBirthday(birthday);
            //计算星座
            base.setConstellation(getConstellation(DateUtil.stringToDate(birthday,"yyyyMMdd")));
            //年龄
            base.setAge(getPersonAgeByBirthDate(birthday));
        }
        if (Tools.notEmpty(page.getEmail())) {
            base.setEmail(page.getEmail());
        }
        if (Tools.notEmpty(page.getClientId())) {
            base.setClientId(page.getClientId());
        }
        if (Tools.notEmpty(page.getPhone())) {
            base.setPhone(page.getPhone());
        }
        if (Tools.notEmpty(page.getSex())) {
            base.setSex(page.getSex());
        }
        if (Tools.notEmpty(page.getHome())) {
            base.setHome(page.getHome());
        }
        if (Tools.notEmpty(page.getImei())) {
            base.setImei(page.getImei());
        }
        if (Tools.notEmpty(page.getInterest())) {
            base.setInterest(page.getInterest());
        }
        if (Tools.notEmpty(page.getIsMarry())) {
            base.setIsMarry(page.getIsMarry());
        }
        if (Tools.notEmpty(page.getLocation())) {
            base.setLocation(page.getLocation());
        }
        if (Tools.notEmpty(page.getModel())) {
            base.setModel(page.getModel());
        }
        if (Tools.notEmpty(page.getOperator())) {
            base.setOperator(page.getOperator());
        }
        if (Tools.notEmpty(page.getOs())) {
            base.setOs(page.getOs());
        }
        if (Tools.notEmpty(page.getProfession())) {
            base.setProfession(page.getProfession());
        }
        if (Tools.notEmpty(page.getQualifications())) {
            base.setQualifications(page.getQualifications());
        }
        /*base.setKeyWords(stringToList(page.getKeyWords()));
        base.setQuestionList(stringToList(page.getQuestionList()));
        base.setTypeList(stringToList(page.getTypeList()));*/
        if (Tools.notEmpty(page.getKeyWords())) {
            base.setKeyWords(page.getKeyWords());
        }
        if (Tools.notEmpty(page.getQuestionList())) {
            base.setKeyWords(page.getQuestionList());
        }
        if (Tools.notEmpty(page.getTypeList())) {
            base.setTypeList(page.getTypeList());
        }
        if (Tools.notEmpty(page.getSumPoint())) {
            base.setSumPoint(page.getSumPoint());
        }
        if (Tools.notEmpty(page.getConsumePonit())) {
            base.setConsumePonit(page.getConsumePonit());
        }
        if (Tools.notEmpty(page.getSumCollection())) {
            base.setSumCollection(page.getSumCollection());
        }
        if (Tools.notEmpty(page.getSumAsk())) {
            base.setSumAsk(page.getSumAsk());
        }
        if (Tools.notEmpty(page.getSumAnswer())) {
            base.setSumAnswer(page.getSumAnswer());
        }
        if (Tools.notEmpty(page.getSumComment())) {
            base.setSumComment(page.getSumComment());
        }
        if (Tools.notEmpty(page.getSumLike())) {
            base.setSumLike(page.getSumLike());
        }
        if (Tools.notEmpty(page.getSumRead())) {
            base.setSumRead(page.getSumRead());
        }
        if (Tools.notEmpty(page.getSumShare())) {
            base.setSumShare(page.getSumShare());
        }
        if (Tools.notEmpty(page.getSumUnInterested())) {
            base.setSumUnLike(page.getSumUnInterested());
        }
        if (Tools.notEmpty(page.getSumUnLike())) {
            base.setSumUnLike(page.getSumUnLike());
        }
        return base;
    }

    public static List<String> stringToList(String str) {

        List<String> strList = new ArrayList<String>();
        if (Tools.notEmpty(str)) {
            String [] arr = str.split(";");
            if (arr != null && arr.length > 0) {
                for (int i = 0; i < arr.length;i++) {
                    strList.add(arr[i]);
                }
            }
        } else {
            return null;
        }
        if (strList.size() > 0) {
            return strList;
        }else {
            return null;
        }

    }

    private static <K, V> Object getMapMaxValue(Map<K, V> map) {
        if (map == null)
            return null;
        Collection<V> c = map.values();
        Object[] obj = c.toArray();
        Arrays.sort(obj);
        return obj[obj.length - 1];
    }

    /**
     * 求主题词的最大权重
     * @param topicList
     * @return
     */
    public static double getMaxWeight(List<DwTopic> topicList) {
        if (topicList == null || topicList.size() <= 0) {
            return 0.0;
        }
        Double [] weightArray = new Double[topicList.size()];
        for (int i = 0; i < topicList.size(); i++) {
            weightArray[i] = topicList.get(i).getTopicWeight();
        }
        Arrays.sort(weightArray);

        return weightArray[weightArray.length - 1];
    }

    /**
     * 归一化，并取前count条数据
     * @param topicList
     * @param maxWeight
     * @param count
     * @return
     */
    public static List<DwTopic> getTopicList(List<DwTopic> topicList,double maxWeight,int count) {
        if (topicList == null || topicList.size() <= 0) {
            return null;
        }
        int size = topicList.size();
        List<DwTopic> topicList2 = new ArrayList<DwTopic>(size);
        for (DwTopic topic : topicList) {
            BigDecimal b =  new BigDecimal(topic.getTopicWeight()/maxWeight);
            double f1 = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
            topic.setTopicWeight(f1);
            topicList2.add(topic);
        }
        if (size <= count) {
            return topicList2;
        }
        Collections.sort(topicList2);
        return topicList2.subList(0,count);
    }


    /**
     * 求分类的最大权重
     * @param categoryList
     * @return
     */
    public static double getCategoryMaxWeight(List<DwCategory> categoryList) {
        if (categoryList == null || categoryList.size() <= 0) {
            return 0.0;
        }
        Double [] weightArray = new Double[categoryList.size()];
        for (int i = 0; i < categoryList.size(); i++) {
            weightArray[i] = categoryList.get(i).getCategroyWeight();
        }
        Arrays.sort(weightArray);
        return weightArray[weightArray.length - 1];
    }
    /**
     * 归一化，并取前count条数据
     * @param categoryList
     * @param maxWeight
     * @param count
     * @return
     */
    public static List<DwCategory> getCategoryList(List<DwCategory> categoryList,double maxWeight,int count) {
        if (categoryList == null || categoryList.size() <= 0) {
            return null;
        }
        int size = categoryList.size();
        List<DwCategory> categoryList2 = new ArrayList<DwCategory>(size);
        for (DwCategory category : categoryList) {
            BigDecimal b =  new BigDecimal(category.getCategroyWeight()/maxWeight);
            double f1 = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
            category.setCategroyWeight(f1);
            categoryList2.add(category);
        }
        if (size <= count) {
            return categoryList2;
        }
        Collections.sort(categoryList2);
        return categoryList2.subList(0,count);
    }

    /**
     * 求分类的最大权重
     * @param channelList
     * @return
     */
    public static double getChannelMaxWeight(List<DwChannel> channelList) {
        if (channelList == null || channelList.size() <= 0) {
            return 0.0;
        }
        Double [] weightArray = new Double[channelList.size()];
        for (int i = 0; i < channelList.size(); i++) {
            weightArray[i] = channelList.get(i).getChannelWeight();
        }
        Arrays.sort(weightArray);
        return weightArray[weightArray.length - 1];
    }
    /**
     * 归一化，并取前count条数据
     * @param channelList
     * @param maxWeight
     * @param count
     * @return
     */
    public static List<DwChannel> getChannelList(List<DwChannel> channelList,double maxWeight,int count) {
        if (channelList == null || channelList.size() <= 0) {
            return null;
        }
        int size = channelList.size();
        List<DwChannel> channelList2 = new ArrayList<DwChannel>(size);
        for (DwChannel channel : channelList) {
            BigDecimal b =  new BigDecimal(channel.getChannelWeight()/maxWeight);
            double f1 = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
            channel.setChannelWeight(f1);
            channelList2.add(channel);
        }
        if (size <= count) {
            return channelList2;
        }
        Collections.sort(channelList2);
        return channelList2.subList(0,count);
    }
    public static String getCategoryName(String categoryCode) {
        for (int i = 0; i < CATEGORYS.size();i++) {
            if (CATEGORYS.get(i).getCategoryCode().equals(categoryCode)) {
                return CATEGORYS.get(i).getCategoryName();
            }
        }
        return null;
    }

    public static final String[] zodiacArr = { "猴", "鸡", "狗", "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊" };

    public static final String[] constellationArr = { "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座" };

    public static final int[] constellationEdgeDay = { 20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22 };

    /**
     * 根据日期获取生肖
     * @return
     */
    public static String getZodica(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return zodiacArr[cal.get(Calendar.YEAR) % 12];
    }

    /**
     * 根据日期获取星座
     * @return
     */
    public static String getConstellation(Date date) {
        if (date == null) {
            return "";
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        if (day < constellationEdgeDay[month]) {
            month = month - 1;
        }
        if (month >= 0) {
            return constellationArr[month];
        }
        // default to return 魔羯
        return constellationArr[11];
    }

    /**
     * 根据出生日期获取人的年龄
     *
     * @param strBirthDate(yyyymmdd)
     * @return
     */
    public static String getPersonAgeByBirthDate(String strBirthDate){

        if ("".equals(strBirthDate) || strBirthDate == null){
            return "";
        }
        //读取当前日期
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DATE);
        //计算年龄
        int age = year - Integer.parseInt(strBirthDate.substring(0, 4)) - 1;
        if (Integer.parseInt(strBirthDate.substring(4,6)) < month) {
            age++;
        } else if (Integer.parseInt(strBirthDate.substring(4,6))== month && Integer.parseInt(strBirthDate.substring(6,8)) <= day){
            age++;
        }
        return String.valueOf(age);
    }

    public static void main(String[] args) {
        System.out.println(getConstellation(new Date()));
        System.out.println(getPersonAgeByBirthDate("19910705"));
    }

}
