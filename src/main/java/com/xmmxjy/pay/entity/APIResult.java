package com.xmmxjy.pay.entity;


/**
 * 返回结果接收类
 * @author Deng
 *
 */
public class APIResult<T> {

	private String result;
	private String message;
	private T info;
	
	
	public T getInfo() {
		return info;
	}
	public void setInfo(T info) {
		this.info = info;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
