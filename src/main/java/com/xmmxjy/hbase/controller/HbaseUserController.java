package com.xmmxjy.hbase.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xmmxjy.common.controller.BaseEndController;
import com.xmmxjy.common.util.AjaxJson;
import com.xmmxjy.common.util.EndUtil;
import com.xmmxjy.common.util.Tools;
import com.xmmxjy.hbase.Constants;
import com.xmmxjy.hbase.entity.*;
import com.xmmxjy.hbase.reposity.DwUserReposity;
import com.xmmxjy.hbase.reposity.UserRepository;
import com.xmmxjy.hbase.util.BeanUtil;
import com.xmmxjy.hbase.util.HBasePageModel;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by xmm on 2017/2/15.
 */
@Controller
@RequestMapping("/hbaseuser")
public class HbaseUserController extends BaseEndController{

    private Logger log = LoggerFactory.getLogger(HbaseUserController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DwUserReposity dwUserReposity;
    @RequestMapping("/test.do")
    public void test(HttpServletRequest request, HttpServletResponse response){
        userRepository.save("1","test@163.com","test");
        List<User> userList = userRepository.findAll();
        System.out.println(userList.size());
    }
    @RequestMapping("/test2.do")
    @ResponseBody
    public Object test2(HttpServletRequest request, HttpServletResponse response){
        List<DwCategory> categoryList = dwUserReposity.findCateGoryList("15236235214");
        return categoryList;
    }
    @RequestMapping("/test3.do")
    @ResponseBody
    public Object test3(HttpServletRequest request, HttpServletResponse response) {
        List<DwUser> userList = dwUserReposity.findUserByCategory("101001");
        return userList;
    }
    @RequestMapping("/userSync.do")
    @ResponseBody
    public AjaxJson userSync(HttpServletRequest request, HttpServletResponse response, DwUserPage userPage,String type) {
        AjaxJson j = new AjaxJson();
        DwBaseUser base = BeanUtil.pageToEntity(userPage);
        if (Tools.isEmpty(type)) {
            j.setMsg("type不能为空！");
            j.setSuccess(false);
            return j;
        }
        if (type.equals(DwUserReposity.TYPE_ADD) || type.equals(DwUserReposity.TYPE_UPDATE)) {
            DwBaseUser user = dwUserReposity.saveBaseUser(base);
            j.setSuccess(true);
            j.setMsg("插入或修改成功！");
        } else if(type.equals(DwUserReposity.TYPE_DELETE)){
            DwBaseUser user = dwUserReposity.deleteBaseUser(base);
            j.setSuccess(true);
            j.setMsg("删除成功！");
        }
    return j;
    }
    @RequestMapping("/toAdd.do")
    public String toAdd(HttpServletRequest request,HttpServletResponse response,ModelMap model) {
        EndUtil.sendEndParams(request,model);
        return END_PAGE + "hbase/" + ADD;
    }
    @RequestMapping("/list.do")
    public String test4(DwBaseUser query, Integer pageNo,Integer pageSize,String startRow,HttpServletRequest request, HttpServletResponse response,ModelMap model) {
        HBasePageModel<DwBaseUser> pageModel = new HBasePageModel<DwBaseUser>(2);
        //List<DwBaseUser> userList2 = dwUserReposity.findBaseUser("",10);
        byte [] startRowArray = null;
        if (Tools.notEmpty(startRow)) {
            startRowArray = Bytes.toBytes(startRow);
        }
            List<DwBaseUser> userList = dwUserReposity.findBaseUser("",startRowArray , null, pageModel).getResultList();

        EndUtil.sendEndParams(request,model);
        model.addAttribute("query",query);
        model.addAttribute("userList",userList);
        model.addAttribute("pageSize",pageModel.getPageSize());
        model.addAttribute("pageSize",pageModel.getPageSize());
        model.addAttribute("pageNo",pageModel.getPageIndex());
        model.addAttribute("startRow1", Bytes.toString(pageModel.getPageStartRowKey()));
        model.addAttribute("startRow2", Bytes.toString(pageModel.getPageEndRowKey()));
        return END_PAGE + "hbase/" + LIST;
    }
    @RequestMapping("/personas.do")
    public String personas(String id,HttpServletRequest request, HttpServletResponse response,ModelMap model){
        DwBaseUser base = dwUserReposity.getBaseUser(id);
        String phone = base.getPhone();
        List<DwTopic> topicList = dwUserReposity.findTopicList(phone);
        List<DwCategory> categoryList = dwUserReposity.findCateGoryList(phone);
        List<DwChannel> channelList = dwUserReposity.findDwChannelList(phone);
        String baseJson = JSON.toJSONString(base);
        String topics = JSON.toJSONString(topicList);
        String categorys = JSON.toJSONString(categoryList);
        String channels = JSON.toJSONString(channelList);
        int maleCount = dwUserReposity.getCountbySex("1");
        int femaleCount = dwUserReposity.getCountbySex("2");
        int otherCount = dwUserReposity.getCountbySex("0");
        JSONArray array = new JSONArray();
        JSONObject sex1 = new JSONObject();
        JSONObject sex2 = new JSONObject();

        JSONObject sex3 = new JSONObject();
        sex1.put("name","男");
        sex1.put("value",maleCount);
        sex2.put("name","女");
        sex2.put("value",femaleCount);
        sex3.put("name","未知");
        sex3.put("value",otherCount);
        array.add(sex1);
        array.add(sex2);
        array.add(sex3);
        int sum = dwUserReposity.getCountUser();
        log.info("sum : " + sum);
        log.info("sexArray :" + array.toJSONString());
        log.info("categorys : " +  categorys);
        log.info("user : " + baseJson);
        model.addAttribute("topics",topics);
        model.addAttribute("categorys",categorys);
        model.addAttribute("channels",channels);
        model.addAttribute("user",baseJson);
        model.addAttribute("sex",array.toJSONString());
        EndUtil.sendEndParams(request,model);
        return END_PAGE + "hbase/" + "personas" ;
    }
    @RequestMapping("/index.do")
    public String sex(HttpServletRequest request, HttpServletResponse response,ModelMap model) {
        //用户不同性别的人数
        int maleCount = dwUserReposity.getCountbySex("1");
        int femaleCount = dwUserReposity.getCountbySex("2");
        int otherCount = dwUserReposity.getCountbySex("0");
        JSONArray array = new JSONArray();
        JSONObject sex1 = new JSONObject();
        JSONObject sex2 = new JSONObject();
        JSONObject sex3 = new JSONObject();
        sex1.put("name","男");
        sex1.put("value",maleCount);
        sex2.put("name","女");
        sex2.put("value",femaleCount);
        sex3.put("name","未知");
        sex3.put("value",otherCount);
        array.add(sex1);
        array.add(sex2);
        array.add(sex3);
        //用户不同年龄的人数
        int age18 = dwUserReposity.getCountAge(Constants.AGE_0,Constants.AGE_18);
        int age30 = dwUserReposity.getCountAge(Constants.AGE_18,Constants.AGE_30);
        int age40 = dwUserReposity.getCountAge(Constants.AGE_30,Constants.AGE_40);
        int age50 = dwUserReposity.getCountAge(Constants.AGE_40,Constants.AGE_50);
        int age60 = dwUserReposity.getCountAge(Constants.AGE_50,Constants.AGE_60);
        int ageMax = dwUserReposity.getCountAge(Constants.AGE_60,Constants.AGE_MAX);
        log.info("age18 " + age18);
        log.info("age30 " + age30);
        log.info("age40 " + age40);
        log.info("age50 " + age50);
        log.info("age60 " + age60);
        log.info("ageMax " + ageMax);
        JSONArray ageArray = new JSONArray();
        JSONObject age18Array = new JSONObject();
        JSONObject age30Array = new JSONObject();
        JSONObject age40Array = new JSONObject();
        JSONObject age50Array = new JSONObject();
        JSONObject age60Array = new JSONObject();
        JSONObject ageMaxArray = new JSONObject();
        age18Array.put("name","18岁以下");
        age18Array.put("value",age18);
        age30Array.put("name","18到30岁");
        age30Array.put("value",age30);
        age40Array.put("name","30到40岁");
        age40Array.put("value",age40);
        age50Array.put("name","40到50岁");
        age50Array.put("value",age50);
        age60Array.put("name","50到60岁");
        age60Array.put("value",age60);
        ageMaxArray.put("name","60岁以上");
        ageMaxArray.put("value",ageMax);
        ageArray.add(age18Array);
        ageArray.add(age30Array);
        ageArray.add(age40Array);
        ageArray.add(age50Array);
        ageArray.add(age60Array);
        ageArray.add(ageMaxArray);
        //不同星座的人数
        //地域展示
        System.out.println("=======" + Thread.currentThread().getContextClassLoader().getResource(""));
        System.out.println(request.getSession().getServletContext().getRealPath("/"));
        System.out.println(System.getProperty("user.dir"));
        JSONArray areaArray = new JSONArray();
        Map<String,String> map =  dwUserReposity.areaList();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            JSONObject o = new JSONObject();
            o.put("name",entry.getKey());
            o.put("value",entry.getValue());
            areaArray.add(o);
        }
        log.info("map :" + map);
        model.addAttribute("map",areaArray.toJSONString());
        model.addAttribute("sex",array.toJSONString());
        model.addAttribute("age",ageArray.toJSONString());
        EndUtil.sendEndParams(request,model);
        return END_PAGE + "hbase/" + "index";
    }

}
