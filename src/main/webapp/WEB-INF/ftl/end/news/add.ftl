<!DOCTYPE html>
<html>
<#include "/end/include/head.ftl"/>
<body style='overflow:scroll;overflow-x:hidden'>
<div class="container bs-docs-container" style="width:100%;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">新增</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="dailogForm" action="${basePath}/news/doAdd.do" method="POST">
                    <input type="hidden" id="btn_sub" class="btn_sub" />
                    <div class="form-group mno">
                        <label for="title" class="col-sm-2 control-label" style="text-align:left;">标题</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" id="title" class="form-control"  datatype="*" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="channel" class="col-sm-2 control-label" style="text-align:left;">种类</label>
                        <div class="col-sm-8">
                            <input type="text" name="channel" id="channel" class="form-control"  datatype="*" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="abstractContent" class="col-sm-2 control-label" style="text-align:left;">摘要</label>
                        <div class="col-sm-8">
                            <input type="text" name="abstractContent" id="abstractContent" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="keywords" class="col-sm-2 control-label" style="text-align:left;">关键词</label>
                        <div class="col-sm-8">
                            <input type="text" name="keywords" id="keywords" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <label for="content" class="col-sm-2 control-label" style="text-align:left;">内容</label>
                        <div class="col-sm-8">
                            <input type="text" name="content" id="content" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group mno">
                        <div class="col-sm-offset-1 col-sm-6">
                            <button type="button" class="btn btn-default" id="formReturn" data-dismiss="modal" onclick="doUrl('${basePath}/news/list.do');">返回</button>
                            <button type="button" class="btn btn-primary" id="formSubmit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="${basePath}/plug-in-ui/project/js/forminit.p3.js"></script>