package com.xmmxjy.common.service;

import com.xmmxjy.system.entity.LogEntity;
import com.xmmxjy.system.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.jms.*;
import javax.sound.midi.Soundbank;

/**
 * Created by xmm on 2017/1/10.
 */
@Component
@Service
public class QueueReceiver implements MessageListener {
    @Autowired
    private LogService logService;

    private static Logger logger = LoggerFactory.getLogger(QueueReceiver.class);

    @Override
    public void onMessage(Message message) {
        try {
            //System.out.println("QueueReceiver1接收到消息:"+((TextMessage)message).getText());
            ObjectMessage objectMessage = (ObjectMessage)message;
            LogEntity log = (LogEntity) objectMessage.getObject();
            logger.info("保存的对象 ：" + log);
            logService.save(log);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
